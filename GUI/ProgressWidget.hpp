/* 
 * File:   ProgressWidget.hpp
 * Author: Dante
 *
 * Created on 7 novembre 2013, 19:30
 */

#ifndef PROGRESSWIDGET_HPP
#define	PROGRESSWIDGET_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include "AbstractGui.hpp"

class ProgressWidget : public AbstractGui {
public:
    ProgressWidget( sf::String windowTitle, sf::String textLabel, sfg::Desktop *desktop, Window *window );
    virtual ~ProgressWidget( );
    
    virtual void buildInterface();
    
    sf::String getTextLabel() const;
    void setTextLabel( const sf::String text );
    
    sf::String getTitle() const;
    void setTitle( const sf::String title );
    
    float getValue();
    void setValue( float value );
    void increaseValueBy( float step );
    void decreaseValueBy( float step );
    
private:
    sfg::Window::Ptr m_progressWindow;
    
    sfg::ProgressBar::Ptr m_progressBar;
    sfg::Label::Ptr m_helpLabel;
    
    sfg::Box::Ptr m_mainBox;
    
    sf::String m_textLabel;  
    sf::String m_windowTitle;

};

#endif	/* PROGRESSWIDGET_HPP */

