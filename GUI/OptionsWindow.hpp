/* 
 * File:   OptionsWindow.hpp
 * Author: shen
 *
 * Created on 9 juillet 2013, 14:25
 */

#ifndef OPTIONSWINDOW_HPP
#define	OPTIONSWINDOW_HPP

#include <iostream>
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include "../Graphic/Window.hpp"
#include "../System/FruitApplication.hpp"
#include "../System/Converter.hpp"

class OptionsWindow
{
public:
    OptionsWindow( sfg::Desktop *desktop, Window *renderWindow );
    virtual ~OptionsWindow();
    
    void buildInterface();
    void initEvents();
    
    void onValidateButtonClick();
    void onApplyButtonClick();
    void closeWindow();
    
    void show();
    void hide();
    
    void resizeViews();
    
    void onSelectResolution();
private:
    sfg::Window::Ptr m_window;
    
    sfg::Desktop *m_desktop;
    Window *m_renderWindow;
    
    sfg::Label::Ptr m_resolutionLabel;
    
    sfg::ComboBox::Ptr m_screenResComboBox;
    sfg::CheckButton::Ptr m_checkboxFullscreen;
    sfg::CheckButton::Ptr m_checkboxVerticalSync;
    
    sfg::Box::Ptr m_mainBox;
    sfg::Box::Ptr m_resolutionBox;
   
    sfg::Box::Ptr m_buttonsBox;
    
    sfg::Button::Ptr m_validateButton;
    sfg::Button::Ptr m_applyButton;
    sfg::Button::Ptr m_cancelButton;
    
    std::vector<sf::VideoMode> m_videoModes;
    
    sfg::ComboBox::IndexType m_originalSelectedMode;
    sfg::ComboBox::IndexType m_currentVideoMode;
};

#endif	/* OPTIONSWINDOW_HPP */

