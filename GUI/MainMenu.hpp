/* 
 * File:   MainMenu.hpp
 * Author: shen
 *
 * Created on 7 juillet 2013, 00:51
 */

#ifndef MAINMENU_HPP
#define	MAINMENU_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include "AbstractGui.hpp"
#include "MapWindow.hpp"

class OptionsWindow;
class MapWindow;
class MainMenu : public AbstractGui
{
public:
    MainMenu( sfg::Desktop *desktop, Window *window );
    virtual ~MainMenu();
    
    virtual void buildInterface();
    void createEvents();
    
    void onQuickGameButtonClick();
    void onQuitButtonClick();
    void onOptionButtonClick();
private:
    sfg::Box::Ptr m_mainBox;
    sfg::Box::Ptr m_headerBox;
    
    sfg::Image::Ptr m_gameIcon;
    sfg::Label::Ptr m_gameTitle;
    sfg::Button::Ptr m_quickGameBtn;
    sfg::Button::Ptr m_multiGameBtn;
    sfg::Button::Ptr m_optionsBtn;
    sfg::Button::Ptr m_quitBtn;
    
    OptionsWindow *m_windowOptions;
    MapWindow *m_mapWindow;
    
};

#endif	/* MAINMENU_HPP */

