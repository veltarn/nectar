/* 
 * File:   AbstractGui.cpp
 * Author: shen
 * 
 * Created on 7 juillet 2013, 00:48
 */

#include "AbstractGui.hpp"

AbstractGui::AbstractGui( sfg::Desktop *desktop, Window *window ) : m_desktop( desktop ), m_window( window )
{
}

AbstractGui::~AbstractGui() {
}

void AbstractGui::freeInterface()
{
    m_desktop->RemoveAll();
}

