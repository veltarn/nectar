/* 
 * File:   ProgressWidget.cpp
 * Author: Dante
 * 
 * Created on 7 novembre 2013, 19:31
 */

#include "ProgressWidget.hpp"

ProgressWidget::ProgressWidget( sf::String windowTitle, sf::String textLabel, sfg::Desktop *desktop, Window *window ) : AbstractGui( desktop, window ), m_windowTitle( windowTitle ), m_textLabel( textLabel )
{
    buildInterface( );
}

ProgressWidget::~ProgressWidget( )
{
}

void ProgressWidget::buildInterface( )
{
    m_progressWindow = sfg::Window::Ptr( sfg::Window::Create( ) );
    

    m_progressBar = sfg::ProgressBar::Ptr( sfg::ProgressBar::Create( ) );
    m_helpLabel = sfg::Label::Ptr( sfg::Label::Create( m_textLabel ) );

    m_mainBox = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::VERTICAL ) );

    m_progressWindow->SetTitle( m_windowTitle );
    m_progressWindow->SetPosition( sf::Vector2f( m_window->getSize( ).x / 2.f - 150.f, 140.f ) );
    
    m_progressBar->SetFraction( 0.0f );

    m_mainBox->Pack( m_helpLabel );
    m_mainBox->Pack( m_progressBar );

    m_progressWindow->Add( m_mainBox );

    m_desktop->Add( m_progressWindow );
}

sf::String ProgressWidget::getTextLabel( ) const
{
    return m_textLabel;
}

void ProgressWidget::setTextLabel( const sf::String text )
{
    m_textLabel = text;
}

sf::String ProgressWidget::getTitle( ) const
{
    return m_windowTitle;
}

void ProgressWidget::setTitle( const sf::String title ) {
    m_windowTitle = title;
}

float ProgressWidget::getValue( ) {
    float v = m_progressBar->GetFraction();
    v = v * 100.f;
    return v;
}

void ProgressWidget::setValue( float value ) {
    float v = value / 100.f;
    m_progressBar->SetFraction( v );
}

void ProgressWidget::increaseValueBy( float step ) {
    float vStep = step / 100.f;
    float cValue = m_progressBar->GetFraction();
    cValue += vStep;
    m_progressBar->SetFraction( cValue );
}

void ProgressWidget::decreaseValueBy( float step ) {
    float vStep = step / 100.f;
    float cValue = m_progressBar->GetFraction();
    cValue -= vStep;
    m_progressBar->SetFraction( cValue );
}