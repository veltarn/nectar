/* 
 * File:   MapWindow.cpp
 * Author: shen
 * 
 * Created on 15 juillet 2013, 14:00
 */

#include "MapWindow.hpp"
#include "../System/FruitApplication.hpp"

using namespace std;

MapWindow::MapWindow( sfg::Desktop *desktop, Window *window ) : m_desktop( desktop ), m_renderWindow( window )
{
    Log *log = Log::getInstance();
    try {
        m_mapManager.loadMapList();
    } catch( FruitException &e ) {
        *log << e.what() << endl;
        
        if( e.getErrorLevel() == FruitException::CRITICAL || e.getErrorLevel() == FruitException::FATAL )
                throw FruitException( 19, "Unable to load maps list", FruitException::CRITICAL );
    }
    
    buildInterface();
    initEvents();
}


MapWindow::~MapWindow() {
}

void MapWindow::buildInterface()
{
    m_mainLayout = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::VERTICAL, 3.0f ) );
    m_buttonsLayout = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::HORIZONTAL ) );
    
    m_window = sfg::Window::Ptr( sfg::Window::Create( ) );
    
    m_mapList = sfg::ComboBox::Ptr( sfg::ComboBox::Create() );
    
    m_playButton = sfg::Button::Ptr( sfg::Button::Create( "Play" ) );
    m_closeButton = sfg::Button::Ptr( sfg::Button::Create( "Close" ) );
    
    m_window->SetTitle( "Level selection" );
    
    m_buttonsLayout->Pack( m_playButton );
    m_buttonsLayout->Pack( m_closeButton );
    
    m_mainLayout->Pack( m_mapList, false, false );
    m_mainLayout->Pack( m_buttonsLayout, true, false );
    
    m_window->Add( m_mainLayout );   
    
    setMapList();
    
    m_desktop->Add( m_window );
    
    m_window->SetPosition( sf::Vector2f( 50.f, 120.f ) );
}

void MapWindow::initEvents()
{
    m_playButton->GetSignal( sfg::Button::OnLeftClick ).Connect( &MapWindow::play, this );
    m_closeButton->GetSignal( sfg::Button::OnLeftClick ).Connect( &MapWindow::closeWindow, this );
}

void MapWindow::closeWindow()
{
    hide();
}

void MapWindow::show()
{
    m_window->Show( true );
}

void MapWindow::hide()
{
    m_window->Show( false );
}

void MapWindow::setMapList()
{
    vector<MapMetadata> mapList = m_mapManager.getMapList();
    
    for( vector<MapMetadata>::iterator it = mapList.begin(); it != mapList.end(); ++it ){
        MapMetadata cMap = *it;
        m_mapList->AppendItem( cMap.mapName );
    }
    /*m_mapList->SetColumnSpacings( 2.0f );
    //Creating table header
    sfg::Label::Ptr screenLabelHeader( sfg::Label::Create( "") );
    sfg::Label::Ptr mapNameHeader( sfg::Label::Create( "Map name" ) );
    sfg::Label::Ptr mapAuthorHeader( sfg::Label::Create( "Author" ) );
    sfg::Label::Ptr maximumPlayers( sfg::Label::Create( "Maximum players" ) );
    sfg::Label::Ptr path( sfg::Label::Create( "Path" ) );
    
    m_mapList->Attach( screenLabelHeader, sf::Rect<sf::Uint32>( 0, 0, 1, 1) );
    m_mapList->Attach( mapNameHeader, sf::Rect<sf::Uint32>( 1, 0, 1, 1) );
    m_mapList->Attach( mapAuthorHeader, sf::Rect<sf::Uint32>( 2, 0, 1, 1) );
    m_mapList->Attach( maximumPlayers, sf::Rect<sf::Uint32>( 3, 0, 1, 1) );
    m_mapList->Attach( path, sf::Rect<sf::Uint32>( 4, 0, 1, 1 ) );
    
    for( int i = 0; i < mapList.size(); ++i )
    {
        MapMetadata cMetadata = mapList.at( i );
        
        bool existingScreenshot = false;
        sf::Image screenshot;
        
        if( screenshot.loadFromFile( cMetadata.screenshotFile ) )
            existingScreenshot = true;
        
        sfg::Image::Ptr screenshotImage( sfg::Image::Create( screenshot ) );
        
        sfg::Label::Ptr mapName( sfg::Label::Create( cMetadata.mapName ) );
        sfg::Label::Ptr mapAuthor( sfg::Label::Create( cMetadata.mapAuthor ) );
        sfg::Label::Ptr maximumPlayers( sfg::Label::Create( Converter::intToString( cMetadata.maximumPlayers ) ) );
        sfg::Label::Ptr mapPath( sfg::Label::Create( cMetadata.mapPath ) );
        sfg::Button::Ptr btnPlay( sfg::Button::Create( "Play" ) );
        
        
        m_mapList->Attach( screenshotImage, sf::Rect<sf::Uint32>( 0, i + 1, 1, 1) );
        m_mapList->Attach( mapName, sf::Rect<sf::Uint32>( 1, i + 1, 1, 1) );
        m_mapList->Attach( mapAuthor, sf::Rect<sf::Uint32>( 2, i + 1, 1, 1 ) );
        m_mapList->Attach( maximumPlayers, sf::Rect<sf::Uint32>( 3, i + 1, 1, 1) );
        m_mapList->Attach( mapPath, sf::Rect<sf::Uint32>( 4, i + 1, 1, 1 ) );
        m_mapList->Attach( btnPlay, sf::Rect<sf::Uint32>( 5, i + 1, 1, 1 ) );
    }*/
}

void MapWindow::play()
{
    std::string selected = m_mapList->GetSelectedText();
    std::string path( "" );
    try 
    {
        path = m_mapManager.getMapPath( selected );
    } catch( FruitException &e ) {
        cerr << "[" << e.getErrorNumber() << "] " << e.what() << endl;
    }
    
    if( !path.empty() )
    {
        FruitApplication *app = FruitApplication::getInstance();
        app->loadGame( path );
        this->hide();
    }
}