/* 
 * File:   OptionsWindow.cpp
 * Author: shen
 * 
 * Created on 9 juillet 2013, 14:25
 */

#include "OptionsWindow.hpp"

using namespace boost::property_tree;

OptionsWindow::OptionsWindow( sfg::Desktop *desktop, Window *renderWindow ) : m_desktop( desktop ), m_renderWindow( renderWindow )
{    
    buildInterface();
    initEvents();
}

OptionsWindow::~OptionsWindow() {
    std::cout << "Destroyed option window" << std::endl;
}

void OptionsWindow::buildInterface()
{    
    File confFile( "data/conf/options.nopt" );
    confFile.open( FileType::JSON );
    
    bool verticalSync = confFile.get<bool>( "VerticalSyncEnabled", false );
    m_window = sfg::Window::Ptr( sfg::Window::Create() );
    
    m_window->SetPosition( sf::Vector2f( m_renderWindow->getSize().x / 2.f - 150, 140 ) );
    m_window->SetTitle( "Options" );
    
    m_mainBox = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::VERTICAL, 1.f ) );
    m_resolutionBox = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::HORIZONTAL, 1.f) );
    m_buttonsBox = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::HORIZONTAL, 1.f) );
    
    m_resolutionLabel = sfg::Label::Ptr( sfg::Label::Create( "Screen resolution !bugged!" ) );
    
    m_validateButton = sfg::Button::Ptr( sfg::Button::Create( "Validate" ) );
    m_applyButton = sfg::Button::Ptr( sfg::Button::Create( "Apply" ) );
    m_cancelButton = sfg::Button::Ptr( sfg::Button::Create( "Cancel" ) );
    
    m_checkboxFullscreen = sfg::CheckButton::Ptr( sfg::CheckButton::Create( "Fullscreen" ) );
    m_checkboxVerticalSync = sfg::CheckButton::Ptr( sfg::CheckButton::Create( "Enable vertical synchronisation ?" ) );
    
    m_checkboxVerticalSync->SetActive( verticalSync );
    m_mainBox->SetSpacing( 3.f );
    
    m_screenResComboBox = sfg::ComboBox::Ptr( sfg::ComboBox::Create() );
    m_videoModes.clear();
    m_videoModes = sf::VideoMode::getFullscreenModes();
    
    FruitApplication *app = FruitApplication::getInstance();
    
    for( std::vector<sf::VideoMode>::iterator it = m_videoModes.begin(); it != m_videoModes.end(); ++it )
    {
        sf::VideoMode cMode = *it;
        std::string strVideoMode = Converter::intToString( cMode.width ) + "x" + Converter::intToString( cMode.height ) + ", " + Converter::intToString( cMode.bitsPerPixel ) + " bits" ;
        
        m_screenResComboBox->AppendItem( strVideoMode );
        
        if( cMode.width == app->getWindow().getSize().x && cMode.height == app->getWindow().getSize().y && cMode.bitsPerPixel == 32)
        {
            m_currentVideoMode = m_screenResComboBox->GetItemCount();
            m_originalSelectedMode = m_currentVideoMode;
            m_screenResComboBox->SelectItem( m_currentVideoMode - 1 );
        }
    }
   
    m_resolutionBox->Pack( m_screenResComboBox );
    m_resolutionBox->Pack( m_resolutionLabel );
    
    m_buttonsBox->Pack( m_validateButton );
    m_buttonsBox->Pack( m_applyButton );
    m_buttonsBox->Pack( m_cancelButton );
    
    m_mainBox->Pack( m_resolutionBox );
    m_mainBox->Pack( m_checkboxFullscreen );
    m_mainBox->Pack( m_checkboxVerticalSync );
    m_mainBox->Pack( m_buttonsBox );
    
    m_window->Add( m_mainBox );   
    
    m_desktop->Add( m_window );
}

void OptionsWindow::initEvents()
{
    m_screenResComboBox->GetSignal( sfg::ComboBox::OnSelect ).Connect( &OptionsWindow::onSelectResolution, this );
    m_validateButton->GetSignal( sfg::Button::OnLeftClick ).Connect( &OptionsWindow::onValidateButtonClick, this );
    m_applyButton->GetSignal( sfg::Button::OnLeftClick ).Connect( &OptionsWindow::onApplyButtonClick, this );
    m_cancelButton->GetSignal( sfg::Button::OnLeftClick ).Connect( &OptionsWindow::closeWindow, this );
}

void OptionsWindow::closeWindow()
{
    hide();
}

void OptionsWindow::show()
{
    m_window->Show( true );
}

void OptionsWindow::hide()
{
    m_window->Show( false );
}

void OptionsWindow::onValidateButtonClick()
{
    //Applying parameters
    onApplyButtonClick();
    
    //Then hide the window
    hide();
}

void OptionsWindow::onApplyButtonClick()
{
    FruitApplication *app = FruitApplication::getInstance();
    File file( "data/conf/options.nopt" );
    file.open( FileType::JSON );
    /*ConfigurationFile confFile( "data/conf/options.nopt" );
    confFile.open();
    
    /*if( m_currentVideoMode - 1 != m_originalSelectedMode )
    {
        sf::VideoMode mode = m_videoModes[m_currentVideoMode - 1];
        //Changing resolution
        app->getWindow().create( mode, app->getApplicationName() );
        resizeViews();
    }*//*
    
    app->getWindow().setVerticalSyncEnabled( m_checkboxVerticalSync->IsActive() );
    //Writing modification on file
    confFile.set( "VerticalSyncEnabled", m_checkboxVerticalSync->IsActive() );
    
    confFile.flush();
    ptree pt;
    read_json( "data/conf/options.nopt", pt );*/
    
    app->getWindow().setVerticalSyncEnabled( m_checkboxVerticalSync->IsActive() );
    //pt.erase( "VerticalSyncEnabled" );
    //pt.add( "VerticalSyncEnabled", m_checkboxVerticalSync->IsActive() );
    file.setValue<bool>( "VerticalSyncEnabled", m_checkboxVerticalSync->IsActive() );
    
    file.saveAsJson();
}

void OptionsWindow::onSelectResolution()
{
    m_currentVideoMode = m_screenResComboBox->GetSelectedItem();
}

void OptionsWindow::resizeViews()
{
        FruitApplication *app = FruitApplication::getInstance();
        Renderer *renderer = Renderer::getInstance();
        
        sf::Vector2u size = app->getWindow().getSize();
                
        sf::View view = renderer->getDrawableGroup( "background" )->getView();
        view.setViewport( sf::FloatRect( view.getViewport().left, view.getViewport().top, static_cast<float>(size.x), static_cast<float>(size.y ) ) );
         renderer->getDrawableGroup( "background" )->setView( view );

        view = renderer->getDrawableGroup( "playground" )->getView();
        view.setViewport(  sf::FloatRect( view.getViewport().left, view.getViewport().top, static_cast<float>(size.x), static_cast<float>(size.y) ) );
        renderer->getDrawableGroup( "playground" )->setView( view );

        view = renderer->getDrawableGroup( "foreground" )->getView();
        view.setViewport(  sf::FloatRect( view.getViewport().left, view.getViewport().top, static_cast<float>(size.x), static_cast<float>(size.y) ) );
        renderer->getDrawableGroup( "foreground" )->setView( view );

        view = renderer->getDrawableGroup( "overlay" )->getView();
        view.setViewport(  sf::FloatRect( view.getViewport().left, view.getViewport().top, static_cast<float>(size.x), static_cast<float>(size.y) ) );
        renderer->getDrawableGroup( "overlay" )->setView( view );

        m_desktop->UpdateViewRect( sf::FloatRect( 0.f, 0.f, size.x, size.y ) );
}