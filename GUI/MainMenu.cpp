/* 
 * File:   MainMenu.cpp
 * Author: shen
 * 
 * Created on 7 juillet 2013, 00:51
 */

#include "MainMenu.hpp"
#include "../System/FruitApplication.hpp"
#include "OptionsWindow.hpp"
#include "MapWindow.hpp"

MainMenu::MainMenu( sfg::Desktop *desktop, Window *window ) : AbstractGui( desktop, window ), m_windowOptions( nullptr ), m_mapWindow( nullptr )
{
    buildInterface();
}

MainMenu::~MainMenu() {
    if( m_windowOptions != nullptr )
    {
        delete m_windowOptions;
        m_windowOptions = nullptr;
    }
}

void MainMenu::buildInterface()
{
    FruitApplication *app = FruitApplication::getInstance();
    m_mainBox = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::VERTICAL, 1.f ) );
    m_headerBox = sfg::Box::Ptr( sfg::Box::Create( sfg::Box::HORIZONTAL, 1.f ) );
    
    
    m_gameIcon = sfg::Image::Ptr( sfg::Image::Create() );
    m_gameTitle = sfg::Label::Ptr( sfg::Label::Create( app->getApplicationName() ) );
    
    m_quickGameBtn = sfg::Button::Ptr( sfg::Button::Create( "Quick Game" ) );
    m_multiGameBtn = sfg::Button::Ptr( sfg::Button::Create( "Multiplayers Game" ) );
    m_optionsBtn = sfg::Button::Ptr( sfg::Button::Create( "Options" ) );
    m_quitBtn = sfg::Button::Ptr( sfg::Button::Create( "Quit" ) );
    
    m_quickGameBtn->SetClass( "mainMenuButtons" );
    m_multiGameBtn->SetClass( "mainMenuButtons" );
    m_optionsBtn->SetClass( "mainMenuButtons" );
    m_quitBtn->SetClass( "mainMenuButtons" );
    
    sf::Image image;
    //This shoud be read from a config file!
    if( !image.loadFromFile( "data/resources/images/nectar_main_icon.png" ) )
    {
        std::cerr << "Cannot load icon image" << std::endl;
    }
    
    m_gameIcon->SetImage( image );
    m_gameTitle->SetClass( "MainMenuLabel" );
    
    m_headerBox->Pack( m_gameIcon );
    m_headerBox->Pack( m_gameTitle );    
    
    m_headerBox->SetPosition( sf::Vector2f( ( m_window->getSize().x / 2.f ) - 175, 75.f ) );
    
    m_mainBox->SetSpacing( 5.0f );
    m_mainBox->Pack( m_quickGameBtn );
    m_mainBox->Pack( m_multiGameBtn );
    m_mainBox->Pack( m_optionsBtn );
    m_mainBox->Pack( m_quitBtn );
    
    createEvents();
    
    sf::Vector2f position( ( m_window->getSize().x / 2.f ) - ( 90), ( m_window->getSize().y / 2.f ) - ( 50.f ) );
    
    m_mainBox->SetPosition( position );
    m_desktop->Add( m_headerBox );
    m_desktop->Add( m_mainBox );
}

void MainMenu::createEvents()
{    
    m_quickGameBtn->GetSignal( sfg::Button::OnLeftClick ).Connect( &MainMenu::onQuickGameButtonClick, this );
    m_optionsBtn->GetSignal( sfg::Button::OnLeftClick ).Connect( &MainMenu::onOptionButtonClick, this );
    m_quitBtn->GetSignal( sfg::Button::OnLeftClick ).Connect( &MainMenu::onQuitButtonClick, this );
}

void MainMenu::onQuickGameButtonClick()
{
    FruitApplication *app = FruitApplication::getInstance();
    Log *log = Log::getInstance();
    
    try
    {
        if( m_mapWindow == nullptr )
            m_mapWindow = new MapWindow( m_desktop, m_window );        
        m_mapWindow->show();
    } catch( FruitException &e ) {
        *log << e.what() << std::endl;
    }    
}

void MainMenu::onQuitButtonClick()
{
    FruitApplication *app = FruitApplication::getInstance();
    
    app->requestShutdown();
}

void MainMenu::onOptionButtonClick()
{
    if( m_windowOptions == nullptr )
        m_windowOptions = new OptionsWindow( m_desktop, m_window );
    
    m_windowOptions->show();
}