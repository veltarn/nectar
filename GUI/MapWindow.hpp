/* 
 * File:   MapWindow.hpp
 * Author: shen
 *
 * Created on 15 juillet 2013, 14:00
 */

#ifndef MAPWINDOW_HPP
#define	MAPWINDOW_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include "../Game/MapManager.hpp"
#include "../Graphic/Window.hpp"
#include "../System/Converter.hpp"

class FruitApplication;
class MapWindow {
public:
    MapWindow( sfg::Desktop *desktop, Window *window);
    virtual ~MapWindow();    
    
    void show();
    void hide();
    void closeWindow();
    void play();
    
    void buildInterface();
    
private:
    //Set list of maps from MapManager to sfg::Table
    void setMapList();
    void initEvents();
private:
    MapManager m_mapManager;
    sfg::Window::Ptr m_window;
    sfg::Box::Ptr m_mainLayout;    
    sfg::Box::Ptr m_buttonsLayout;
    //sfg::Table::Ptr m_mapList;
    sfg::ComboBox::Ptr m_mapList;
    
    sfg::Button::Ptr m_playButton;
    sfg::Button::Ptr m_closeButton;
    
    sfg::Desktop *m_desktop;
    Window *m_renderWindow;
};

#endif	/* MAPWINDOW_HPP */

