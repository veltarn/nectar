/* 
 * File:   AbstractGui.hpp
 * Author: shen
 *
 * Created on 7 juillet 2013, 00:48
 */

#ifndef ABSTRACTGUI_HPP
#define	ABSTRACTGUI_HPP

#include <iostream>
#include <SFGUI/SFGUI.hpp>
#include "../Graphic/Window.hpp"

class AbstractGui {
public:
    AbstractGui( sfg::Desktop *desktop, Window *window );
    virtual ~AbstractGui();
    
    virtual void buildInterface() = 0;
    virtual void freeInterface();
protected:
    sfg::Desktop *m_desktop;
    Window *m_window;
    
};

#endif	/* ABSTRACTGUI_HPP */

