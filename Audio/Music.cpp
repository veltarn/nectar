/* 
 * File:   Music.cpp
 * Author: shen
 * 
 * Created on 6 novembre 2013, 19:44
 */

#include "Music.hpp"

using namespace std;

void Music::openFromFile( const string& filename ) {
    m_musicStream.openStream( filename );
    
    if( !m_musicStream.isOpen() ) {
        throw FruitException( 21, "Cannot open stream \"" + filename + "\"", FruitException::CRITICAL );
    }
    
    openFromStream( m_musicStream );
}
