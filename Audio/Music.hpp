/* 
 * File:   Music.hpp
 * Author: shen
 *
 * Created on 6 novembre 2013, 19:44
 */

#ifndef MUSIC_HPP
#define	MUSIC_HPP

#include <iostream>
#include <SFML/Audio.hpp>
#include "../System/PhysFsStream.hpp"
#include "../System/FruitException.hpp"

class Music : public sf::Music {
public:
    /**
     * Inhériting only this method in order to read into zip files
     * @param filename
     */
    virtual void openFromFile( const std::string &filename );
private:
    PhysFsStream m_musicStream;

};

#endif	/* MUSIC_HPP */

