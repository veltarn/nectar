/* 
 * File:   MapWriter.hpp
 * Author: Dante
 *
 * Created on 12 juin 2013, 00:32
 */

#ifndef MAPWRITER_HPP
#define	MAPWRITER_HPP

#include <iostream>
#include <fstream>
#include <cmath>
#include <SFML/System.hpp>
#include <Box2D/Box2D.h>
//#include <json/json.h>

#include "../System/Size.hpp"
#include "../System/FruitException.hpp"

class MapWriter {
public:
    MapWriter( );
    MapWriter( std::string mapname, std::string filename );
    virtual ~MapWriter( );
    
    void setFileName( std::string filename );
    void setMapName( std::string mapname );
    
    //void writeFlatMap();
private:
    std::string m_mapName;
    std::string m_fileName;
    /*Json::StyledStreamWriter m_jsonStream;
    Json::Value m_root;*/
};

#endif	/* MAPWRITER_HPP */

