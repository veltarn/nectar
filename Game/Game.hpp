/* 
 * File:   Game.hpp
 * Author: shen
 *
 * Created on 8 juillet 2013, 14:09
 */

#ifndef GAME_HPP
#define	GAME_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <functional>
#include "../GUI/ProgressWidget.hpp"
#include "World.hpp"
#include "Map.hpp"
#include "../System/FruitApplication.hpp"
#include "../System/FruitException.hpp"
#include "../System/Log.hpp"
#include "../Graphic/Renderer.hpp"

class Game
{
public:
    Game( );
    virtual ~Game();
    
    void setMapFile( std::string fileName );
    std::string getMapFile() const;
    
    void loadMap( ProgressWidget *progressWidget );
    void unloadMap();
    
    void start();
    void pause();
    void unPause();
    void stopGame();
    
    bool isMapLoaded() const;
    
    World *getWorld() const;
    
    void update( sf::Time frametime );
private:
    std::string m_mapFile;
    
    bool m_isMapLoaded;
    bool m_playing;
    Log *m_log;
    
    World *m_world;
};

#endif	/* GAME_HPP */

