/* 
 * File:   MapManager.hpp
 * Author: shen
 *
 * Created on 15 juillet 2013, 14:02
 */

#ifndef MAPMANAGER_HPP
#define	MAPMANAGER_HPP

#include <iostream>
#include <vector>
#include "../System/PhysFs.hpp"
#include "../System/FileInfo.h"
#include "../System/Log.hpp"
#include "Map.hpp"
#include "MapWriter.hpp"
#include "../System/Dir.hpp"
#include "../System/Pak.h"
#include "../System/FruitException.hpp"

class MapManager 
{
public:
    MapManager();
    virtual ~MapManager();
    
    void loadMapList();
    std::vector<MapMetadata> &getMapList();
    
    /**
     * Find the map Path with the mapName into m_mapList
     * @param mapName Name of the map
     * @throw FruitException if no map has been found
     * @return Path of the selected map
     */
    std::string getMapPath( std::string mapName );
private:
    std::vector<MapMetadata> m_mapList;
};

#endif	/* MAPMANAGER_HPP */

