/* 
 * File:   MapManager.cpp
 * Author: shen
 * 
 * Created on 15 juillet 2013, 14:02
 */

#include "MapManager.hpp"

using namespace std;

MapManager::MapManager() {
}


MapManager::~MapManager() {
}

void MapManager::loadMapList()
{
    PhysFs::PhysFs *physFs = PhysFs::PhysFs::getInstance();
    Log *log = Log::getInstance();
    
    string rootMapsPath( "data/maps/" );
    Dir mapDir( rootMapsPath );
    vector<string> filter;
    filter.push_back( ".nel" );
    vector<string> pathList = mapDir.entryList( Dir::FILES, filter );
    
    for( int i = 0; i < pathList.size(); ++i )
    {
        string cMap = pathList.at( i );
        string fullPath = rootMapsPath + cMap;
        FileInfo info( cMap );
        string subdirMap( "maps/" + info.baseName() );
        
        if( !physFs->mountTmp( fullPath, subdirMap, true ) ) {
            throw FruitException( 17, "Cannot add \"" + fullPath + "\" to virtual directory \"" + subdirMap + "\".\nReason: " + physFs->getLastError(), FruitException::CRITICAL );            
        }
        
        StringList list = physFs->enumerateFiles( "/temp/maps/ToonCountry/music" );
        for( StringList::iterator it = list.begin(); it != list.end(); ++it ) {
            cout << *it << endl;
        }
        MapMetadata metaData;
        metaData = Map::getMetadata( physFs->getMountPoint( fullPath ), fullPath );
        
        m_mapList.push_back( metaData );
        
        physFs->removeFromSearchPath( fullPath );
    }
}

vector<MapMetadata> &MapManager::getMapList()
{
    return m_mapList;
}

std::string MapManager::getMapPath( std::string mapName )
{
    for( vector<MapMetadata>::iterator it = m_mapList.begin(); it != m_mapList.end(); ++it ) {
        MapMetadata cMap = *it;
        if( cMap.mapName == mapName ) {
            cout << cMap.mapPath << endl;
            return cMap.mapPath;
        }
    }
    
    throw FruitException( 14, "Cannot find map path into the list...", FruitException::CRITICAL );
}