/* 
 * File:   Geth.hpp
 * Author: Dante
 *
 * Created on 16 juin 2013, 19:22
 */

#ifndef GETH_HPP
#define	GETH_HPP

#include <iostream>
#include <vector>
#include <SFML/System.hpp>
#include <Box2D/Box2D.h>
#include "World.hpp"
#include "../System/Log.hpp"
#include "../System/FruitException.hpp"
#include "../System/Size.hpp"
#include "../System/typenames.hpp"

/**
 * This class is for convenience.
 * It's help to the edge detection process
 * 
 * Pour une raison inconnue, il faut rajouter 544.63 aux coordonnées de pixels
 * pour les synchroniser correctement
 */
class Geth
{
public:
    enum Direction
    {
        LEFT = 0,
        TOP = 1,
        RIGHT = 2,
        BOTTOM = 3
    };
    
    Geth( sf::Vector2i position, const MapData_t &worldDefinition, World *world, Size groundSize, Size mapSize );
    virtual ~Geth( );
    
    int getCurrentBlock() const;
    int getBlock( Geth::Direction direction );
    
    /**
     * Move the bot, return false until it reachs the right of the level or the original point
     * @param worldDefinition
     * @return 
     */
    bool move( const MapData_t &worldDefinition );
    
    b2Vec2 *getVerticesNetwork();
    unsigned int getVerticesNumber() const;
private:
    void init( const MapData_t &worldDefinition );
    
private:
    sf::Vector2i m_position;
    sf::Vector2i m_originalPosition;;
    Geth::Direction m_currentDirection;
    Size m_groundSize;
    Size m_mapSize;
    
    int m_currentBlock;
    /**
     * Thoses variables are useful to detect which blocs sorround the current bloc
     * -1 value is set if nothing represents the bloc (out of worldDefinition bounds)
     */
    int m_topBlock;
    int m_leftBlock;
    int m_rightBlock;
    int m_bottomBlock;
    int m_topLeftCornerBlock;
    int m_topRightCornerBlock;
    int m_bottomLeftCornerBlock;
    int m_bottomRightCornerBlock;
    
    std::vector<b2Vec2> m_verticesNetwork;
    
    World *m_world;
    sf::Clock m_clock;
    Log *m_log;
};

#endif	/* GETH_HPP */

