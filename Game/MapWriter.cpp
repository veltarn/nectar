/* 
 * File:   MapWriter.cpp
 * Author: Dante
 * 
 * Created on 12 juin 2013, 00:32
 */

#include "MapWriter.hpp"

using namespace std;

MapWriter::MapWriter( )
{
}

MapWriter::MapWriter( string mapname, string filename ) : m_fileName( filename ), m_mapName( mapname )
{
}

MapWriter::~MapWriter( )
{
}

void MapWriter::setFileName( std::string filename )
{
    m_fileName = filename;
}

void MapWriter::setMapName( std::string mapname )
{
    m_mapName = mapname;
}

/*void MapWriter::writeFlatMap()
{
    cout << "Writing basic map" << endl;
    if( m_fileName.empty() )
    {
        throw FruitException( 4, "No file was selected for write", FruitException::WARNING );
    }
    
    if( m_mapName.empty() )
    {
        cerr << "No map name defined, \"Untitled\" will be chosed" << endl;
        m_mapName = "Untitled";
    }
    
    ofstream file( m_fileName.c_str() );
    
    m_root["name"] = m_mapName;
    
    Json::Value backgroundArray;
    backgroundArray.append( Json::Value( "data/resources/backgrounds/sea.png" ) );
    backgroundArray.append( Json::Value( "data/resources/backgrounds/beach.png" ) );
    m_root["backgrounds"] = backgroundArray;
    
    Json::Value ressources;
    ressources["barrel"] = "data/resources/sprites/barrel.png";
    ressources["wood"] =  "data/resources/sprites/wood.png";
    ressources["mapMusic"] =  "data/resources/music/level.ogg";
    m_root["resources"] = ressources;
    
    Json::Value mapSize;
    mapSize["width"] = 5468;
    mapSize["height"] = 3252;
    
    m_root["worldSize"] = mapSize;
    
    m_root["maximumPlayers"] = 25;
    
    int nbBlocksWidth = ceil( 5468.f / 16.f );
    int nbBlocksHeight = ceil( 3252 / 16 );
    
    Json::Value mapDefinition;
    for( int y = nbBlocksHeight; y > 0; --y )
    {
        Json::Value row;
        for( int x = 0; x < nbBlocksWidth; ++x )
        {
            int pcentY = ( y * 100 ) / nbBlocksHeight;
            
            if( pcentY <= 25 )
            {
                row.append( Json::Value( 1 ) );
            }
            else
            {
                row.append( Json::Value( 0 ) );
            }
        }
        mapDefinition.append( row );
    }
    
    m_root["mapDefinition"] =  mapDefinition;
    
    m_jsonStream.write( file, m_root );
    
    cout << "Done" << endl;
}*/