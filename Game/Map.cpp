/* 
 * File:   Map.cpp
 * Author: Dante
 * 
 * Created on 10 juin 2013, 19:29
 */

#include "Map.hpp"

using namespace std;
using namespace boost::property_tree;

Map::Map( World *world ) : m_mapSize( 0, 0 ), m_world( world ), m_groundPosition( 0, 0 )
{
    init();
}

Map::Map( string fileName, World *world ) : m_mapSize( 0, 0 ), m_world( world ), m_groundPosition( 0, 0 )
{
    m_metadata.mapPath = fileName;
    init();
}

Map::~Map( )
{
    m_mapMusic.stop();
    *m_log << "Map instance destroyed" << endl;
}

void Map::init()
{
    m_log = Log::getInstance();    
    m_textureManager = TextureManager::getInstance();
    m_physFs = PhysFs::PhysFs::getInstance();
    m_world->setMap( this );
}

void Map::setMapFile( std::string fileName )
{
    m_metadata.mapPath = fileName;
}

void Map::parseMapFile( ProgressWidget *progressWidget )
{
    *m_log << "Loading map " << m_metadata.mapPath << endl;    
    sf::Clock clock;
    //Mounting map on temporary directory
    string vPath = "/temp/maps/currentlyPlayed/" + m_metadata.mapName;
    m_currentVirtualMapPath = vPath;
    if( !m_physFs->mount( m_metadata.mapPath, vPath, true ) ) {
        throw FruitException( 20, "Cannot mount \"" + m_metadata.mapPath + "\" on virtual directory \"" + vPath + "\"", FruitException::CRITICAL );
    }
    
    //Json::Value root;
    /*PAK binFile;
    try {
        binFile.read( m_metadata.mapPath );
    } catch ( PakFileException &e ) {
        cerr << e.what() << endl;
    }
    string fileName = binFile.extract( "level.json", "level.json" );*/
    string tmpPath( vPath + "/level.json" );
    PhysFs::IfStream ifStream( tmpPath );
    
    ptree pt;
    read_json( ifStream, pt );
    /*ifstream file( fileName, ios::in );
    if( !m_mapFile.parse( file, root, false ) )
    {
        std::string error = "Unable to parse \"";
        error += m_metadata.mapPath.c_str();
        error += "\" map file.\n";
        error += m_mapFile.getFormatedErrorMessages().c_str();

        throw FruitException( 1, error, FruitException::CRITICAL );
    }*/
    //string mapName = root.get( "name", "Untitled" ).asString();    
    string mapName( pt.get<string>( "name", "Untitled" ) );
    cout << "Mn Size: " << mapName.size() << ", " << mapName << endl;
    //mapName += "\0";
    //const Json::Value backgrounds = root["backgrounds"];
    try
    {
        BOOST_FOREACH( ptree::value_type &value, pt.get_child( "backgrounds" ) )
            m_backgroundPaths.push_back( value.second.data() );
    } catch( boost::property_tree::ptree_bad_path &e ) {
        cerr << e.what() << endl;
    }
    m_musicPath = pt.get<string>( "music", "" );
    cout << m_musicPath << endl;
    int wSizeX = pt.get<int>( "worldSize.width" );
    int wSizeY = pt.get<int>( "worldSize.height" );
    
    map<std::string, std::string> sky;
    //const Json::Value sky = root["skyColor"];
    BOOST_FOREACH( ptree::value_type &value, pt.get_child( "skyColor" ) ) {
        string key = value.second.get<string>( "pos" );
        string valueC = value.second.get<string>( "value" );
        sky[key] = valueC;
    }
    
    m_metadata.maximumPlayers = pt.get<int>( "maximumPlayers" );
    
    //Getting map size
    m_mapSize.setWidth( wSizeX );
    m_mapSize.setHeight( wSizeY );    
    progressWidget->increaseValueBy( 12 );
    createSky( sky );
    progressWidget->increaseValueBy( 26 );
    //loadRessources( resources );
    
    // @todo: Générer dynamiquement les backgrounds
    
    /**
     * Loading map's tiles
     */

    if( m_mapSize.getWidth() == 0 || m_mapSize.getHeight() == 0 )
        throw FruitException( 2, "The map size cannot be equals to 0", FruitException::CRITICAL );
    else if ( m_mapSize.getWidth() >= MAX_MAP_SIZE || m_mapSize.getHeight() >= MAX_MAP_SIZE ) {
        std::cerr << "The map size cannot be superior or equal to " << MAX_MAP_SIZE << "\nMap will be brutally resized..." << std::endl;

        if( m_mapSize.getWidth() >= MAX_MAP_SIZE )
            m_mapSize.setWidth( MAX_MAP_SIZE - 1 );

        if( m_mapSize.getHeight() >= MAX_MAP_SIZE )
            m_mapSize.setHeight( MAX_MAP_SIZE - 1 );
    }
    
    //Converting worlddef to bi-dimensionnal vector
    MapData_t worldDefinition;
    
    BOOST_FOREACH( ptree::value_type &value, pt.get_child( "mapDefinition" ) ) {
        vector<int> row;
        BOOST_FOREACH( ptree::value_type &rValue, value.second ) {
            row.push_back( Converter::stringToInt( rValue.second.data() ) );
        }
        worldDefinition.push_back( row );
    }
    
    progressWidget->increaseValueBy( 25 );
    
    cout << m_mapSize.getWidth() << " / " << m_mapSize.getHeight() << endl;
    cout << "WorldDefinition Size: Y: " << worldDefinition.size() << " X: " << worldDefinition[0].size()  << endl;
    createGround( worldDefinition );
    
    progressWidget->increaseValueBy( 50 );
    
    initializeMusic();
    
    progressWidget->increaseValueBy( 12 );
    //Renderer *renderer = Renderer::getInstance(); 
    //Fonctionne si commenté...
    m_metadata.mapName = mapName;
    cout << "Map \"" << mapName << "\" read in " << clock.getElapsedTime().asMilliseconds() << " ms" << endl;
}

void Map::createGround( MapData_t &worldDefinition )
{
    cout << "World Size: " << worldDefinition[0].size() << " / " << worldDefinition.size() << endl;
    sf::Image groundImage;
    
    sf::Image ground = m_textureManager->get( "data/resources/textures/ground/ground.png" ).copyToImage();
    sf::Image groundSurface = m_textureManager->get( "data/resources/textures/ground/surface_ground.png" ).copyToImage();
    
    Size groundSize = getGroundSize( worldDefinition );
    
    groundImage.create( groundSize.m_width, groundSize.m_height, sf::Color( 0, 0, 0, 0 ) );
    int nonGroundSize = m_mapSize.m_height - groundSize.m_height;
    
    // crossing worldDefinition array
    for( int i = 0; i < worldDefinition.size(); ++i )
    {
        std::vector<int> row = worldDefinition.at( i );
        
        //unsigned int yPixel = i % groundImage.getSize().y;
        int nonGroundXOffset = 0;
        for( int j = 0; j < row.size(); ++j )
        {            
            //unsigned int xPixel = j % groundImage.getSize().x;
            int number = row[j];
            
            int yGroundTextureOffset = ( ( i + 1 ) - ceil( nonGroundSize / 16 ) ) * 16;
            int xGroundTextureOffset = j * 16;
            
            // If number equals 1, we copy the ground unit texture into groundImage
            if( number == 1 )
            {                   
                if( isSurfaceGroundCase( worldDefinition, j, i ) )
                {
                    groundImage.copy( groundSurface, xGroundTextureOffset, yGroundTextureOffset, sf::IntRect( 0, 0, 0, 0 ), true );
                }
                else
                {                
                    sf::Vector2i imageToCopyOffset;
                    imageToCopyOffset.x = ( ( j * 16 ) % ground.getSize().x ) + 16;
                    imageToCopyOffset.y = ( ( i * 16 ) % ground.getSize().y ) + 16;
                    sf::IntRect sourceRect( imageToCopyOffset.x, imageToCopyOffset.y, 16, 16 );
                    groundImage.copy( ground, xGroundTextureOffset, yGroundTextureOffset, sourceRect, true );
                }
            }
            else
            {
                nonGroundXOffset++;
            }
        }
    }    
    
    b2Body *body = createPhysicalBounds( worldDefinition, groundSize );
    //cout << body->GetPosition().x * m_world->getWorldScale() << " / " << body->GetPosition().y * m_world->getWorldScale() << endl;
    sf::Texture groundTexture;
    groundTexture.loadFromImage( groundImage );
    
    m_textureManager->add( "ground", groundTexture );
    
        
    //Creating ground
    sf::Vector2f groundPosition( 0, m_mapSize.m_height - groundSize.m_height);
    //groundPosition.y = m_mapSize.m_height - groundSize.m_height;
    
    //groundPosition.y = body->GetPosition().y * m_world->getWorldScale();
    
    m_groundEntity = new GroundEntity( m_world );
    m_groundEntity->setBody( body );
    //groundEntity->setPosition( groundPosition );
    //cout << body->GetPosition().x * m_world->getWorldScale() << " / " << body->GetPosition().y * m_world->getWorldScale() << endl;
    m_groundEntity->setTexture( &m_textureManager->get( "ground" ) );
    //cout << groundEntity->getOrigin().x << " / " << groundEntity->getOrigin().y << endl;
    
    boost::mutex mtx;
    mtx.lock();
    Renderer *renderer = Renderer::getInstance(); 
    //Adding ground to world and renderer
    renderer->addEntity( "playground", m_groundEntity );
    m_world->addEntity( m_groundEntity );
    mtx.unlock();
}

b2Body *Map::createPhysicalBounds( const MapData_t& worldDefinition, Size groundSize )
{
    *m_log << "Creating ground's physics" << endl;
    
    sf::Vector2i firstBlockPos = getFirstLeftGroundBlock( worldDefinition );
    
    //Saving ground position in the attribute
    m_groundPosition.x = firstBlockPos.x * 16;
    m_groundPosition.y = firstBlockPos.y * 16;
    
    firstBlockPos.y -= 1;
    
    std::vector<int> row = worldDefinition.at( firstBlockPos.y );
    
    Geth geth( firstBlockPos, worldDefinition, m_world, groundSize, m_mapSize );
    
    bool running = true;
    while( running )
    {
        if( geth.move( worldDefinition ) )
        {
            running = false;
        }
    }
    
    b2Vec2 *vertices = geth.getVerticesNetwork();
    
    b2ChainShape chain;
    chain.CreateChain( vertices, geth.getVerticesNumber() );
    
    //Creating body
    b2FixtureDef fixture;
    fixture.shape = &chain;
    fixture.density = 1.f;
    fixture.friction = 0.3f;
    fixture.restitution = 0.35f;
    
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position = b2Vec2( ( firstBlockPos.x * 16 ) / m_world->getWorldScale(), ( ( firstBlockPos.y * 16 ) ) / m_world->getWorldScale() );
    
    b2Body *body = m_world->CreateBody( &bodyDef );
    body->CreateFixture( &fixture );
    
    *m_log << "Done" << endl;
    
    return body;
}

sf::Vector2i Map::getFirstLeftGroundBlock( const MapData_t& worldDefinition )
{
    if( worldDefinition.size() == 0 )
    {
        throw FruitException( 6, "Unable to locate bloc because worldDefinition is empty", FruitException::CRITICAL );
    }
    unsigned int a = 0;
    std::vector<int> row = worldDefinition.at( a );
    
    for( int x = 0; x < row.size(); ++x )
    {
        for( int y = 0; y < worldDefinition.size(); ++y )
        {
            std::vector<int> cRow = worldDefinition.at( y );
            int number = cRow[x];
            
            if( number == 1 )
            {
                sf::Vector2i nPos( x, y );
                return nPos;
            }
        }
    }
}

Size Map::getGroundSize( MapData_t &worldDefinition )
{
    Size groundSize(0, 0);
    
    
    int yStartOffset = 0;
    int yEndOffset = worldDefinition.size();
    bool yCalculated = false;
    
    for( int i = 0; i < worldDefinition.size() && !yCalculated; ++i )
    {
        std::vector<int> row = worldDefinition.at( i );
        int j = 0;
        int number = 0;
        /*int xStartOffset = 0;
        int xEndOffset = 0;*/
        //Prévoir les cas 1, 2, 3 si je continue comme ça..;
        while( j < row.size() && number != 1 )
        {
            number = row[j];
            if( number == 1 )
            {
                //xStartOffset = j + 1;
                if(!yCalculated)
                {
                        yStartOffset = i + 1;
                        yCalculated = true;
                }
            }
            ++j;
        }
        
        /*j = row.size() - 1;
        number = 0;
        while( j > 0 && number != 1 )
        {
            number = row[j].asInt();
            if( number == 1 )
            {
                xEndOffset = j + 1;
            }
            --j;
        }
        
        if( xEndOffset > 0)
        {
            int width = ( xEndOffset * 16 ) - ( xStartOffset * 16 );
            if(yCalculated)
            {
                int height = ( yEndOffset * 16 ) - ( yStartOffset * 16 );
                groundSize.m_height = height;
            }
            groundSize.m_width = width;
            
        }*/
        if( yCalculated )
        {
            int height = ( yEndOffset * 16 ) - ( yStartOffset * 16 );
            groundSize.m_height = height;
        }
    }
    groundSize.m_width = m_mapSize.m_width;
    return groundSize;
}

bool Map::isSurfaceGroundCase( MapData_t& worldDefinition, int xCoord, int yCoord )
{
    if( ( yCoord - 1 ) < 0 )
        return true;
    else if( yCoord > worldDefinition.size() )
        return false;
    
    //Getting row array
    vector<int> row = worldDefinition.at( yCoord - 1 );
    
    if( xCoord < 0 || xCoord >= row.size() )
        return false;
    
    int number = row[xCoord];
    
    if( number != 1 )
        return true;
    else
        return false;
}

/*void Map::loadRessources( const Json::Value& resources )
{
    for( int i = 0; i < resources.size(); ++i )
    {
        sf::Texture texture;
        texture.loadFromFile( resources[i].asString());
        
        m_textureManager->add( resources[i].asString(), texture );
    }
}*/

void Map::createSky( const map<string, string> sky )
{    
    Sky *skyEntity = new Sky( sky, m_mapSize, m_world, false );
    
    boost::mutex mtx;
    mtx.lock();
    
    Renderer *renderer = Renderer::getInstance();
    
    renderer->addEntity( "background" , skyEntity );
    mtx.unlock();
}

string Map::getMapName() const
{
    return m_metadata.mapName;
}

string Map::getMapPath() const {
    return m_metadata.mapPath;
}

sf::Vector2f Map::getGroundPosition() const
{
    return m_groundPosition;
}

MapMetadata Map::getMetadata( string filepath, string realPath )
{
    //cout << filepath << endl;
    MapMetadata metadata;
    metadata.mapPath = realPath;
    
    PhysFs::PhysFs *phys = PhysFs::PhysFs::getInstance();
    cout << phys->getMountPoint( filepath ) << endl;
    
    string fullpath( filepath + "level.json" );
    PhysFs::IfStream ifStream( fullpath );
    /*try
    {
        
        
                
        char *content = binFile.getFile( "level.json" );
        if( content != NULL ) {
            fileContent = content;
        }
    } catch( FruitException &e ) {
        cerr << e.what() << endl;
    }
    */
    //istringstream stream( fileContent );
    ptree pt;
    
    try {        
        read_json( ifStream, pt );
    } catch( ptree_bad_data &e ) {
        cerr << e.what() << endl;
    } catch( ptree_bad_path &e ) {
        cerr << e.what() << endl;
    }
    
    
    string screenPath = filepath;
    size_t lastDot = screenPath.find_last_of( '.' );
    screenPath = screenPath.substr( 0, lastDot );
    screenPath += ".png";
    
    metadata.mapName = pt.get<string>( "name", "Untitled" );
    metadata.mapAuthor = pt.get<string>( "author", "no author" );
    metadata.maximumPlayers = pt.get<int>( "maximumPlayers", 4 );
    metadata.screenshotFile = screenPath;
    return metadata;
}

void Map::initializeMusic() {
    string mPath = m_currentVirtualMapPath + m_musicPath;
    cout << mPath << endl;
    
    m_mapMusic.openFromFile( mPath );
    m_mapMusic.play();
}