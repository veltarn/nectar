/* 
 * File:   World.hpp
 * Author: Dante
 *
 * Created on 9 juin 2013, 15:31
 */

#ifndef WORLD_HPP
#define	WORLD_HPP

#include <iostream>
#include <list>
#include <SFML/System.hpp>
#include <Box2D/Box2D.h>
#include "../Graphic/PhysicEntity.hpp"
#include "../System/Size.hpp"
#include "../System/Converter.hpp"
#include "../System/Log.hpp"
#include "Map.hpp"

#define WORLD_SCALE (float)32.f

class Entity;
class Map;
class World : public b2World
{
public:
    World( sf::Vector2f gravity );
    virtual ~World( );

    float getWorldScale();
    
    void setScreenOffset( const sf::Vector2f offset );
    void setScreenOffset ( const float offsetX, const float offsetY );
    sf::Vector2f getScreenOffset() const;
    
    void update( sf::Time &elapsedTime );
    
    void addEntity( PhysicEntity *entity );
    void removeEntity( PhysicEntity *entity );
    
    void setMap( Map *map );
    Map *getMap() const;
    
    b2Body *createBox( sf::Vector2f position, sf::Vector2f size, b2BodyType bodyType );
    b2Body *createBox( float x, float y, float width, float height, b2BodyType bodyType);
private:
    Size m_worldSize;
    b2Body *m_worldBoundaries;
    float m_worldScale;
    Log *m_log;
    sf::Vector2f m_screenOffset;
    
    Map *m_map;
    
    std::list<PhysicEntity*> m_entitiesList;
};

#endif	/* WORLD_HPP */

