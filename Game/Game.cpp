/* 
 * File:   Game.cpp
 * Author: shen
 * 
 * Created on 8 juillet 2013, 14:09
 */

#include "Game.hpp"

using namespace std;

Game::Game()  : m_world( nullptr )
{
    m_log = Log::getInstance();
}

Game::~Game() {
    if( m_world != nullptr )
    {
        delete m_world;
        m_world = nullptr;
    }
}

void Game::loadMap( ProgressWidget *progressWidget )
{
    PhysFs::PhysFs *physfs = PhysFs::PhysFs::getInstance();
    cout << physfs->getMountPoint( m_mapFile ) << endl;
    *m_log << "Loading map \"" << m_mapFile << "\"" << endl;
    //Setting basic gravity (could change during map loading)
    sf::Vector2f gravity( 0.f, 9.8f );
    if( m_world != nullptr )
    {
        delete m_world;
        m_world = nullptr;
    }
    
    m_world = new World( gravity );
    
    Map *map = new Map( m_mapFile, m_world );
    progressWidget->increaseValueBy( 1 );
    //Loading map
    try
    {
        map->parseMapFile( progressWidget );
    }
    catch( FruitException &e )
    {
        *m_log << e.what() << endl;
        throw FruitException( 9, "Cannot load map \"" + m_mapFile, FruitException::CRITICAL );
    }
    m_world->setMap( map );
    
    FruitApplication *app = FruitApplication::getInstance();
    
    app->loadedMap();
    m_isMapLoaded = true;
}

void Game::unloadMap()
{
    PhysFs::PhysFs *physfs = PhysFs::PhysFs::getInstance();
    
    Map *map = m_world->getMap();
    Renderer *renderer = Renderer::getInstance();
    FruitApplication *app = FruitApplication::getInstance();
    
    *m_log << "Unloading map \"" << map->getMapName() << endl;
    string mapName = map->getMapPath();
    physfs->removeFromSearchPath( mapName );
    m_world->setMap( nullptr );
    delete m_world;
    m_world = nullptr;
    
    delete map;
    map = nullptr;
    
    renderer->clearDrawableGroupContents( "background" );
    renderer->clearDrawableGroupContents( "playground" );
    renderer->clearDrawableGroupContents( "foreground" );    
    
    m_isMapLoaded = false;
    app->unloadedMap();    
}

World *Game::getWorld() const
{
    return m_world;
}

void Game::update( sf::Time frametime )
{
    m_world->update( frametime );
}

void Game::setMapFile( std::string fileName )
{
    m_mapFile = fileName;
}

std::string Game::getMapFile() const
{
    return m_mapFile;
}