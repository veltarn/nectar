/* 
 * File:   Map.hpp
 * Author: Dante
 *
 * Created on 10 juin 2013, 19:29
 */

#ifndef MAP_HPP
#define	MAP_HPP

#define MAX_MAP_SIZE 27000
#define BLOCK_SIZE 16

#include <iostream>
#include <istream>
#include <cmath>
#include <vector>
#include <map>
#include <fstream>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/foreach.hpp>
#include "../System/typenames.hpp"
#include "../System/TextureManager.hpp"
#include "../System/Log.hpp"
#include "../System/FruitException.hpp"
#include "../Graphic/Renderer.hpp"
#include "../System/Size.hpp"
#include "../Game/World.hpp"
#include "../Graphic/GroundEntity.hpp"
#include "../Graphic/Sky.hpp"
#include "../System/PhysFs.hpp"
#include "../Audio/Music.hpp"
#include "../System/Pak.h"
#include "../System/PakFileException.hpp"
#include "../System/Converter.hpp"
#include "../GUI/ProgressWidget.hpp"
#include "Geth.hpp"



enum ENTITIES_TYPE
{
    VOID_ = 0,
    GROUND = 1,
    SURFACE = 2,
    CAVE_SURFACE = 3,
    TREE = 4,
    PLANT = 5,
    BRIDGE = 6,
    TOWER = 7,
    ROCK = 8
};

struct MapMetadata
{
    std::string mapName;
    std::string mapAuthor;
    unsigned int maximumPlayers;
    std::string mapPath;
    std::string screenshotFile;
};

class World;
class Map {
public:
    Map( World *world );
    Map( std::string fileName, World *world );
    virtual ~Map( );

    void setMapFile( std::string fileName );

    /**
     * This method parses a JSON file which represents a map
     * @throws FruitException if the file cannot be parsed
     */
    void parseMapFile( ProgressWidget *progressWidget );
    
    std::string getMapName() const;
    std::string getMapPath() const;
    sf::Vector2f getGroundPosition() const;
    
    Size getMapSize() const
    {
        return m_mapSize;
    }
    
    // Used to quickly access to map metadata from the outside
    static MapMetadata getMetadata( std::string filepath, std::string realPath);
private:
    void init();
    /**
     * Creates ground with numeric informations of the worldDefinition.
     * This method dynamicaly creates the ground's texture, binary physic image and send this to GroundEntity
     * @param worldDefinition
     */
    void createGround( MapData_t &worldDefinition );
    
    /**
     * Returns true if the ground case is a surface case
     * @param worldDefinition reference to worldDefinition array
     * @return true if the ground case is a surface case, false otherwise
     */
    bool isSurfaceGroundCase( MapData_t &worldDefinition, int xCoord, int yCoord );
    
    /**
     * This method calculate the ground size by reading the array
     * @param worldDefinition
     * @return 
     */
    Size getGroundSize( MapData_t &worldDefinition );
    
    b2Body *createPhysicalBounds( const MapData_t &worldDefinition, Size groundSize );
    
    sf::Vector2i getFirstLeftGroundBlock( const MapData_t &worldDefinition );
    
    //void loadRessources( const Json::Value &resources );
    
    void createSky( const std::map<std::string, std::string> sky );
    void initializeMusic();
private:
    std::string m_currentVirtualMapPath;
    
    Size m_mapSize;
    MapMetadata m_metadata; //Contains map name, map path, maximum players and map author
    
    Log *m_log;
    
    TextureManager *m_textureManager;
    
    GroundEntity *m_groundEntity;
    sf::Vector2f m_groundPosition;
    std::vector<std::string> m_backgroundPaths;
    std::string m_musicPath;
    
    World *m_world;
    PhysFs::PhysFs *m_physFs;
    
    Music m_mapMusic;
};

#endif	/* MAP_HPP */

