/* 
 * File:   World.cpp
 * Author: Dante
 * 
 * Created on 9 juin 2013, 15:31
 */

#include "World.hpp"

using namespace std;

World::World( sf::Vector2f gravity ) : b2World( Converter::vector2fToB2Vec2( gravity ) ), m_map( nullptr ), m_worldScale( WORLD_SCALE )
{
    m_log = Log::getInstance();
    *m_log << "[World] World initialised" << endl;
}


World::~World( )
{
    if( m_map != nullptr )
    {
        delete m_map;
        m_map = nullptr;
    }
}

float World::getWorldScale()
{
    return m_worldScale;
}

void World::setScreenOffset( const sf::Vector2f offset )
{
    m_screenOffset = offset;
}

void World::setScreenOffset( const float offsetX, const float offsetY )
{
    setScreenOffset( sf::Vector2f( offsetX, offsetY ) );
}

sf::Vector2f World::getScreenOffset() const
{
    return m_screenOffset;
}

void World::update( sf::Time& elapsedTime )
{
    float timestep = 1.0f / ( 1.0f / elapsedTime.asSeconds() );
    Step( timestep, 8, 3 );
    
    //Updating positions
    for( list<PhysicEntity*>::iterator it = m_entitiesList.begin(); it != m_entitiesList.end(); ++it )
    {
        //cout << (*it)->getBody()->GetPosition().y * m_worldScale << " / " << (*it)->getPosition().y << endl;
        //(*it)->update( elapsedTime );
        b2Body *body = (*it)->getBody();
        sf::Vector2f newPos = Converter::b2Vec2ToVec2( body->GetPosition(), m_worldScale );
        //cout << newPos.x << " / " << newPos.y << endl;
        (*it)->setPosition( newPos );
        (*it)->setRotation( ( body->GetAngle() * 180.f ) / b2_pi );
    }
}

void World::addEntity( PhysicEntity* entity )
{
    *m_log << "[World] Add entity to list" << endl;
    m_entitiesList.push_back( entity );
}

void World::removeEntity( PhysicEntity* entity )
{
    bool found = false;
    for( list<PhysicEntity*>::iterator it = m_entitiesList.begin(); it != m_entitiesList.end() && !found; ++it )
    {
        if( *it == entity )
        {
            found = true;
            m_entitiesList.erase( it );
            *m_log << "[World] Removed entity from list" << endl;
        }
    }
}

void World::setMap( Map* map )
{
    m_map = map;
}

Map *World::getMap() const
{
    return m_map;
}

b2Body *World::createBox( sf::Vector2f position, sf::Vector2f size, b2BodyType bodyType ) {
    b2BodyDef bodyDef;
    bodyDef.position = Converter::vector2fToB2Vec2( position, m_worldScale );
    bodyDef.type = bodyType;
    b2Body *body = CreateBody( &bodyDef );
    
    b2PolygonShape shape;
    shape.SetAsBox( ( size.x / 2 ) / m_worldScale, ( size.y / 2 ) / m_worldScale );
    b2FixtureDef fixture;
    fixture.density = 1.f;
    fixture.friction = 0.7f;
    fixture.restitution = 0.35f;
    fixture.shape = &shape;
    body->CreateFixture( &fixture );
    return body;
}

b2Body *World::createBox( float x, float y, float width, float height, b2BodyType bodyType) {
    return createBox( sf::Vector2f( x, y ), sf::Vector2f( width, height ), bodyType );
}