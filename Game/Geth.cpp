/* 
 * File:   Geth.cpp
 * Author: Dante
 * 
 * Created on 16 juin 2013, 19:22
 */

#include "Geth.hpp"

using namespace std;

Geth::Geth( sf::Vector2i position, const MapData_t &worldDefinition, World *world, Size groundSize, Size mapSize ) :
        m_position( position ),
        m_originalPosition( position ),
        m_currentDirection( Geth::RIGHT ),
        m_currentBlock( -1 ),
        m_topBlock( -1 ),
        m_leftBlock( -1 ),
        m_rightBlock( -1 ),
        m_bottomBlock( -1 ),
        m_topLeftCornerBlock( -1 ),
        m_topRightCornerBlock( -1 ),
        m_bottomLeftCornerBlock( -1 ),
        m_bottomRightCornerBlock( -1 ),
        m_world( world ),
        m_groundSize( groundSize ),
        m_mapSize( mapSize )
{
    //Putting first vertice
    cout << world->getScreenOffset().x << " / " << world->getScreenOffset().y << endl;
    b2Vec2 vec( ( ( m_position.x * 16 ) - world->getScreenOffset().x ) / m_world->getWorldScale(), ( ( ( m_position.y * 16 ) - ( m_mapSize.m_height - m_groundSize.m_height ) + 16 )/* + 544.63f */) / m_world->getWorldScale() );
    //b2Vec2 vec( ( ( m_position.x * 16 ) - world->getScreenOffset().x ) / m_world->getWorldScale(), ( ( m_position.y * 16 ) + 16 ) / m_world->getWorldScale() );
    //b2Vec2 vec( (m_position.x * 16 ) / m_world->getWorldScale(), ( ( ( m_position.y * 16 ) + 544.63f ) / m_world->getWorldScale() ) );
    //b2Vec2 vec( 0, 544 / m_world->getWorldScale() );
    m_verticesNetwork.push_back( vec );
    
    init( worldDefinition );
}

Geth::~Geth( )
{
}

void Geth::init( const MapData_t &worldDefinition )
{
    m_log = Log::getInstance();
    *m_log << "[Geth] Starting edge detection bot" << endl;
    std::vector<int> row = worldDefinition.at( m_position.y );
    
    //Getting top block
    if( m_position.y - 1 >= 0 )
    {
        std::vector<int> tRow = worldDefinition.at( m_position.y - 1 );
        m_topBlock = tRow[m_position.x];
        
        //getting TLC block
        if( m_position.x - 1 >= 0)
        {
            m_topLeftCornerBlock = tRow[m_position.x - 1];
        }
        
        //Getting TRC block
        if( m_position.x + 1 < tRow.size() )
        {
            m_topRightCornerBlock = tRow[m_position.x + 1];
        }
    }
    
    //Getting left block
    if( m_position.x - 1 >= 0 )
    {
        m_leftBlock = row[m_position.x - 1];
    }
    
    //Getting Bottom block
    if( m_position.y + 1 < worldDefinition.size() )
    {
        std::vector<int> bRow = worldDefinition.at( m_position.y + 1 );
        m_bottomBlock = bRow[m_position.x];
        
        //Getting BLC block
        if( m_position.x - 1 >= 0 )
        {
            m_bottomLeftCornerBlock = bRow[m_position.x - 1];
        }
        
        //Getting BRC block
        if( m_position.x + 1 < bRow.size() )
        {
            m_bottomRightCornerBlock = bRow[m_position.x + 1];
        }
    }
    
    //Getting right block
    if( m_position.x + 1 < row.size() )
    {
        m_rightBlock = row[m_position.x + 1];
    }
}

int Geth::getCurrentBlock() const
{
    return m_currentBlock;
}

int Geth::getBlock( Geth::Direction direction )
{    
    int value = -1;
    switch( direction )
    {
        case Geth::LEFT:
            value = m_leftBlock;
        break;
        
        case Geth::RIGHT:
            value = m_rightBlock;
        break;
        
        case Geth::TOP:
            value = m_topBlock;
        break;
        
        case Geth::BOTTOM:
            value = m_bottomBlock;
        break;
            
        default:
            value = m_currentBlock;
         break;
    }
    return value;
}

bool Geth::move( const MapData_t &worldDefinition )
{
    std::vector<int> row = worldDefinition.at( m_position.y );
    
    //std::cout << "Cur pos: " << m_position.x << " / " << m_position.y << ", " << m_currentDirection << std::endl;
    
    switch( m_currentDirection )
    {
        case Geth::LEFT:
            if( m_leftBlock == 1 )
            {
                if( m_position.y + 1 < worldDefinition.size() )
                {
                    m_currentDirection = Geth::BOTTOM;
                }
            }
            else if( m_topBlock != 1 )
            {
                if( m_position.y - 1 >= 0 )
                {
                    m_position.y -= 1;
                    m_currentDirection = Geth::TOP;
                }
            }
            else
            {
                if( m_position.x - 1 >= 0 )
                {
                    m_position.x -= 1;
                }
            }
        break;
        
        case Geth::RIGHT:
            if( m_rightBlock == 1 ) // If way is blocked by a cube
            {
                if( m_position.y - 1 >= 0 )
                {
                    m_currentDirection = Geth::TOP;
                }
            }
            else if( m_bottomBlock != 1) // If there is no ground
            {
                if( m_position.y + 1 < worldDefinition.size() )
                {
                    m_position.y += 1;
                    m_currentDirection = Geth::BOTTOM;
                }
            }
            else
            {
                if( m_position.x + 1 < row.size() )
                {
                    m_position.x += 1;
                }
            }
        break;
        
        case Geth::TOP:
            if( m_topBlock == 1 )
            {   
                if( m_position.x - 1 >= 0 )
                {
                    m_currentDirection = Geth::LEFT;
                }
            }
            else if( m_rightBlock != 1 )
            {
                if( m_position.x + 1 < row.size() )
                {
                    m_position.x += 1;
                    m_currentDirection = Geth::RIGHT;
                }
            }
            else
            {
                if( m_position.y - 1 >= 0 )
                {
                    m_position.y -= 1;
                }
            }
        break;
        
        case Geth::BOTTOM:
            if( m_bottomBlock == 1 )
            {
                if( m_position.x + 1 <= row.size() )
                {
                    m_currentDirection = Geth::RIGHT;
                }
            }
            else if( m_leftBlock != 1 )
            {
                if( m_position.x - 1 > 0 )
                {
                    m_position.x -= 1;
                    m_currentDirection = Geth::LEFT;
                }
            }
            else
            {
                if( m_position.y + 1 < worldDefinition.size() )
                {
                    m_position.y += 1;
                }
            }
        break;
    }
    
    //Update row and column
    row = worldDefinition.at( m_position.y );
    
    //Update current block
    m_currentBlock = row[m_position.x];
    
    //Update top block
    if( m_position.y - 1 >= 0 )
    {
        std::vector<int> topRow = worldDefinition.at( m_position.y - 1 );
        m_topBlock = topRow[m_position.x];
    }
    
    //Update left block
    if( m_position.x - 1 > 0 )
    {
        m_leftBlock = row[m_position.x - 1];
    }
    
    //Update bottom block
    if( m_position.y + 1 < worldDefinition.size() )
    {
        std::vector<int> bottomRow = worldDefinition.at( m_position.y + 1 );
        m_bottomBlock = bottomRow[m_position.x];
    }
    
    //Update right block
    if( m_position.x + 1 < row.size() )
    {
        m_rightBlock = row[m_position.x + 1];
    }
    
    //Update TLC block
    if( m_position.x - 1 > 0 && m_position.y - 1 > 0 )
    {
        std::vector<int> tlcRow = worldDefinition.at( m_position.y - 1 );
        m_topLeftCornerBlock = tlcRow[m_position.x - 1];
    }
    
    //Update TRC block
    if( m_position.x + 1 < row.size() && m_position.y - 1 > 0 )
    {
        std::vector<int> trcRow = worldDefinition.at( m_position.y - 1 );
        m_topRightCornerBlock = trcRow[m_position.x + 1];
    }
    
    //Update BLC block
    if( m_position.x - 1 > 0 && m_position.y + 1 < worldDefinition.size() )
    {
        std::vector<int> blcRow = worldDefinition.at( m_position.y + 1 );
        m_bottomLeftCornerBlock = blcRow[m_position.x - 1];
    }
    
    //Update BRC block
    if( m_position.x + 1 < row.size() && m_position.y + 1 < worldDefinition.size() )
    {
        std::vector<int> brcRow = worldDefinition.at( m_position.y + 1 );
        m_bottomRightCornerBlock = brcRow[m_position.x + 1];
    }
    
    //Creating vertices if necessary
    b2Vec2 vertex;
    switch( m_currentDirection )
    {
        case Geth::LEFT:
            if( ( m_topLeftCornerBlock != 1 || m_leftBlock == 1 ) && m_topBlock == 1 )
            {
                vertex.x = ( m_position.x * 16 ) / m_world->getWorldScale();
                //vertex.y = ( m_position.y * 16 ) / m_world->getWorldScale();
                vertex.y = 544 / m_world->getWorldScale();
                m_verticesNetwork.push_back( vertex );
                //cout << "[LEFT]Vertex set at " << vertex.x << "x" << vertex.y << endl;
            }
        break;
            
        case Geth::RIGHT:
            if( ( ( m_bottomRightCornerBlock != 1 || m_rightBlock == 1 ) && m_bottomBlock == 1 ) || m_position.x + 1 >= row.size() )
            {
                vertex.x = ( ( m_position.x * 16 ) + 16 ) / m_world->getWorldScale();
                //vertex.y = ( ( m_position.y * 16 ) + 16 ) / m_world->getWorldScale();
                vertex.y = ( ( ( m_position.y * 16 ) - ( m_mapSize.m_height - m_groundSize.m_height ) + 16  ) /*+ 544.63f*/ ) / m_world->getWorldScale();
                //vertex.y = 544 / m_world->getWorldScale();
                m_verticesNetwork.push_back( vertex );
                //cout << "[RIGHT]Vertex set at " << vertex.x << "x" << vertex.y << endl;
            }
        break;
            
        case Geth::TOP:
            if( ( m_topRightCornerBlock != 1 || m_topBlock == 1 ) && m_rightBlock == 1 )
            {
                vertex.x = ( ( m_position.x * 16 ) + 16 ) / m_world->getWorldScale();
                //vertex.y = ( m_position.y * 16 ) / m_world->getWorldScale();
                vertex.y = ( ( ( m_position.y * 16 ) - ( m_mapSize.m_height - m_groundSize.m_height ) ) /*+ 544.63f*/ ) / m_world->getWorldScale();
                //vertex.y = 544 / m_world->getWorldScale();
                m_verticesNetwork.push_back( vertex );
                //cout << "[Top]Vertex set at " << vertex.x << "x" << vertex.y << endl;
            }
        break;
            
        case Geth::BOTTOM:
            if( ( ( m_bottomLeftCornerBlock != 1 || m_bottomBlock == 1 ) && m_leftBlock == 1 ) )
            {
                vertex.x = ( m_position.x * 16 ) / m_world->getWorldScale();
                //vertex.y = ( ( m_position.y * 16 ) + 16 ) / m_world->getWorldScale();
                vertex.y = ( ( ( m_position.y * 16 ) - ( m_mapSize.m_height - m_groundSize.m_height ) + 16 ) /*+ 544.63f*/ ) / m_world->getWorldScale();
                //vertex.y = 544 / m_world->getWorldScale();
                m_verticesNetwork.push_back( vertex );
                //cout << "[BOTTOM]Vertex set at " << vertex.x << "x" << vertex.y << endl;
            }
        break;
    }
    
    /**
     * if current position + 1 will be out of array bounds or if position has returned to the original position (in cavern-like levels cases)
     * we return true, otherwise false
     */
    if( m_position.x + 1 >= row.size() || m_position == m_originalPosition )
    {
        *m_log << "[Geth] Physical map determined in " << m_clock.getElapsedTime().asMilliseconds() << " ms" << endl;
        return true;
    }
    else
    {
        return false;
    }
}

b2Vec2 *Geth::getVerticesNetwork()
{
    b2Vec2 *verticesArray;
    
    verticesArray = new b2Vec2[m_verticesNetwork.size()];
    
    for( int i = 0; i < m_verticesNetwork.size(); ++i )
    {
        verticesArray[i] = m_verticesNetwork[i];
    }
    
    return verticesArray;
}

unsigned int Geth::getVerticesNumber() const
{
    return m_verticesNetwork.size();
}