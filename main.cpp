/* 
 * File:   main.cpp
 * Author: Dante
 *
 * Created on 30 mai 2013, 18:29
 */

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "System/FruitException.hpp"
#include "System/FruitApplication.hpp"
#include "Game/World.hpp"
#include "Game/Map.hpp"
#include "Game/MapWriter.hpp"
#include "Graphic/Renderer.hpp"
#include "System/TextureManager.hpp"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    FruitApplication *nectar = FruitApplication::getInstance();
    
    nectar->init( "Nectar" );
    /*Map map( "maMap.json", nectar.getWorld() );
    map.parseMapFile();*/
    

    nectar->run();
    
    nectar->kill();
    return 0;
}