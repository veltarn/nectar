/* 
 * File:   Box.cpp
 * Author: Dante
 * 
 * Created on 19 octobre 2013, 15:50
 */

#include "Box.hpp"
#include "../Game/World.hpp"

Box::Box( sf::Vector2f pos, World *world ) : PhysicEntity( world ), m_shape( sf::Vector2f( 30.f, 30.f ) )
{
    setOrigin( 30.f / 2, 30.f / 2 );
    setPosition( pos );
    m_body = world->createBox( pos, sf::Vector2f( 30.f, 30.f ), b2_dynamicBody );
}


Box::~Box( )
{
}

void Box::draw( sf::RenderTarget& target, sf::RenderStates states ) const {
    target.draw( m_shape, states );
}

void Box::setPosition( const sf::Vector2f &position ) {
    m_shape.setPosition( position );
}

void Box::setPosition( float x, float y ) {
    setPosition( sf::Vector2f( x, y ) );
}