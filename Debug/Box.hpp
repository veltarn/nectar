/* 
 * File:   Box.hpp
 * Author: Dante
 *
 * Created on 19 octobre 2013, 15:50
 */

#ifndef BOX_HPP
#define	BOX_HPP

#include "../Graphic/PhysicEntity.hpp"

class World;
class Box : public PhysicEntity {
public:
    Box( sf::Vector2f pos, World *world );
    virtual ~Box( );
    
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
    virtual void setPosition( const sf::Vector2f &position );
    virtual void setPosition( float x, float y );
private:
    sf::RectangleShape m_shape;
};

#endif	/* BOX_HPP */

