/* 
 * File:   ClockHud.hpp
 * Author: Dante
 *
 * Created on 3 juillet 2013, 15:25
 */

#ifndef CLOCKHUD_HPP
#define	CLOCKHUD_HPP

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include <SFML/Graphics.hpp>
#include "Entity.hpp"
#include "../System/FrameClock.hpp"

class ClockHud : public Entity
{
public:
    
    struct Stat
    {
        sf::Color color;
        std::string str;
    };
    typedef std::vector<Stat> Stats;
    
    ClockHud( const FrameClock *clock, const sf::Font &font );
    
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
    void show();
    void hide();
    bool isHidden() const;
private:
    template<typename T>
    static std::string format( std::string name, std::string unit, T value )
    {
        std::ostringstream os;
        os.precision(4);
        os << std::left << std::setw(5);
        os << name << " : ";
        os << std::setw(5);
        os << value << " " << unit;
        return os.str();
    }
    
    Stats build() const;
private:
    const sf::Font m_font;
    const FrameClock *m_clock;
    
    bool m_hidden;
};

#endif	/* CLOCKHUD_HPP */

