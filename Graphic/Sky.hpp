/* 
 * File:   Sky.hpp
 * Author: Dante
 *
 * Created on 25 juin 2013, 19:45
 */

#ifndef SKY_HPP
#define	SKY_HPP

#include <iostream>
#include <vector>
#include <cmath>
#include <map>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include "Entity.hpp"
#include "../Game/World.hpp"
#include "../System/Size.hpp"
#include "../System/Log.hpp"
#include "../System/FruitException.hpp"
#include "../System/Converter.hpp"
#include "../System/TextureManager.hpp"

class World;
class Sky : public Entity
{
public:
    Sky( std::map< std::string, std::string> skyGradient, const Size mapSize, World *world, bool generateClouds );
    virtual ~Sky( );
    
    void generateSky( const Size mapSize );
    
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
private:
    void initSky( const Size mapSize);
    void calculateGradient( std::map< std::string, std::string > skyGradient, Size mapSize );
private:
    World *m_world;
    sf::VertexArray m_vertices;
    sf::Texture *m_skyTexture;
    
    Log *m_log;
    std::vector< sf::Color > m_skyGradient;
    
    bool m_generateClouds;
};

#endif	/* SKY_HPP */

