/* 
 * File:   BreakableEntity.cpp
 * Author: Dante
 * 
 * Created on 12 juin 2013, 20:20
 */

#include "BreakableEntity.hpp"
#include "../Game/World.hpp"

BreakableEntity::BreakableEntity( World *world ) : PhysicEntity( world ), m_health( 0 ), m_maximumHealth( 0 )
{
}


BreakableEntity::BreakableEntity( unsigned int currentHealth, unsigned int maximumHealth, World *world ) : PhysicEntity( world ), m_health( currentHealth ), m_maximumHealth( maximumHealth )
{}

BreakableEntity::~BreakableEntity( )
{
}

void BreakableEntity::update( sf::Time delta )
{
    if( m_health <= 0 )
    {
        onDestroy();
        m_toDelete = true;
    } else {
        sf::Vector2f position = Converter::b2Vec2ToVec2( m_body->GetPosition() );
       //Rescaling position to SFML values
       position.x *= m_world->getWorldScale();
       position.y *= m_world->getWorldScale();

       float angle = m_body->GetAngle() * 180.f / b2_pi;

       setPosition( position );
       setRotation( angle );
    }
}