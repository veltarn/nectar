/* 
 * File:   Renderer.hpp
 * Author: Dante
 *
 * Created on 17 juin 2013, 23:10
 */

#ifndef RENDERER_HPP
#define	RENDERER_HPP

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "../System/Log.hpp"
#include "DrawableGroup.hpp"
#include "Entity.hpp"
#include "../System/Size.hpp"
#include "../System/Converter.hpp"
#include "../System/singleton.hpp"

/*class Entity;
class DrawableGroup;*/
class Renderer : public sf::Drawable, public Singleton<Renderer>
{
    friend class Singleton<Renderer>;
public:
    void init();
    void addDrawableGroup( std::string name );
    void deleteDrawableGroup( std::string name );
    DrawableGroup *getDrawableGroup( std::string name ) const;
    
    void addEntity( std::string groupName, Entity *drawable );
    void removeEntity( std::string groupName, Entity *drawable );
    
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
    
    sf::View &getView( std::string groupName );
    
    void clearDrawableGroupContents( std::string drawableGroupName );
protected:        
    virtual ~Renderer( );
private:
    std::vector <DrawableGroup*> m_drawableGroups;
    Log *m_log;
};

#endif	/* RENDERER_HPP */

