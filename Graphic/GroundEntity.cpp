/* 
 * File:   GroundEntity.cpp
 * Author: Dante
 * 
 * Created on 13 juin 2013, 02:07
 */

#include "GroundEntity.hpp"
#include "../Game/World.hpp"

using namespace std;

GroundEntity::GroundEntity( World *world ) : PhysicEntity( world )
{
    //Creating vertices array
    m_vertices.setPrimitiveType( sf::Quads );
    m_vertices.resize( 4 );
}

GroundEntity::~GroundEntity( )
{
}

void GroundEntity::setTexture( sf::Texture* texture )
{       
    m_texture = texture;    
    sf::Vector2u textSize = m_texture->getSize();
    
    m_vertices[0].position = sf::Vector2f( getPosition().x, getPosition().y );
    m_vertices[1].position = sf::Vector2f( getPosition().x + textSize.x, getPosition().y );
    m_vertices[2].position = sf::Vector2f( getPosition().x + textSize.x, getPosition().y + textSize.y );
    m_vertices[3].position = sf::Vector2f( getPosition().x, getPosition().y + textSize.y );
    
    m_vertices[0].texCoords = sf::Vector2f( 0, 0 );
    m_vertices[1].texCoords = sf::Vector2f( textSize.x, 0 );
    m_vertices[2].texCoords = sf::Vector2f( textSize.x, textSize.y );
    m_vertices[3].texCoords = sf::Vector2f( 0, textSize.y );
    
    setOrigin( getPosition().x, getPosition().y );
}

sf::Texture *GroundEntity::getTexture() const
{
    return m_texture;
}

void GroundEntity::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    //for( m_body.get)
    //cout << this->getPosition().x << " / " << this->getPosition().y << endl;
    states.transform *= getTransform();
    states.texture = m_texture;
    target.draw( m_vertices, states );
}

void GroundEntity::setPosition( const sf::Vector2f& position )
{
    b2Vec2 vec;
    /*vec.x = position.x / m_world->getWorldScale();
    vec.y = position.y / m_world->getWorldScale();*/
    vec.x = ( position.x / m_world->getWorldScale() ) - m_world->getScreenOffset().x;
    vec.y = -( position.y / m_world->getWorldScale() ) + m_world->getScreenOffset().y;
    //m_body->SetTransform( vec, m_body->GetAngle() );
    
    /*b2Fixture *fixtures = m_body->GetFixtureList();
    
    b2ChainShape *shape = dynamic_cast<b2ChainShape*>(fixtures[0].GetShape());
    b2Vec2 *vertices = shape->m_vertices;
    for( int i = 0; i < shape->m_count; ++i )
    {
        b2Vec2 v = vertices[i];
        cout << v.x << " / " << v.y * m_world->getWorldScale() << endl;
    }*/
    
    //cout << getPosition().x << " / " << getPosition().y << " | " << m_body->GetPosition().x << " x " << m_body->GetPosition().y * m_world->getWorldScale() << endl;
    sf::Transformable::setPosition( position );
}