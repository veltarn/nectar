/* 
 * File:   Entity.hpp
 * Author: Dante
 *
 * Created on 12 juin 2013, 19:33
 */

#ifndef ENTITY_HPP
#define	ENTITY_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include "../System/Converter.hpp"

class Entity : public sf::Drawable, public sf::Transformable
{
public:   
    
    Entity();
    virtual ~Entity( );
    
    //virtual void update( sf::Time delta );
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const = 0;
    
    
    virtual void setPosition( const sf::Vector2f &position );
    virtual void setPosition( float x, float y );
    
protected:
    
    bool m_toDelete;
};

#endif	/* ENTITY_HPP */

