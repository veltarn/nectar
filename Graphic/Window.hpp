/* 
 * File:   Window.h
 * Author: Dante
 *
 * Created on 29 mars 2013, 23:18
 */

#ifndef WINDOW_H
#define	WINDOW_H

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Renderer.hpp"
#include "../System/Log.hpp"


class Window : public sf::RenderWindow{
public:
    Window();
    Window(sf::VideoMode vm, const std::string &title);
    virtual ~Window();

    //Récupère la position de la caméra (taille et position)
    sf::FloatRect getViewportPosition();
    sf::FloatRect getViewportPosition(sf::View view);
    void draw(const sf::Drawable &drawable, const sf::RenderStates &states = sf::RenderStates::Default);
private:
    Log *m_log;
};
#endif	/* WINDOW_H */

