/* 
 * File:   PhysicEntity.hpp
 * Author: Dante
 *
 * Created on 25 juin 2013, 20:02
 */

#ifndef PHYSICENTITY_HPP
#define	PHYSICENTITY_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include "../System/Converter.hpp"
/*#include "../Game/World.hpp"
*/
#include "Entity.hpp"

class World;
class PhysicEntity : public Entity
{
public:
        
    enum EntityCategory
    {
        BOUNDARY =              0x0001,
        CHARACTER =             0x0002,
        FOREGROUND_ENTITY =     0x0004,
        BACKGROUND_ENTITY =     0x0008,
        NON_COLLIDABLE_ENTITY = 0x0010, // e.g a grenade
        COLLIDABLE_ENTITY =     0x0020, // e.g a rocket
        GROUND =                0x0040
    };
    
    PhysicEntity( World *world );
    virtual ~PhysicEntity( );
    
    
    void setBody( b2Body *body );
    b2Body *getBody() const;    
    b2BodyType getBodyType() const; //Static, dynamic or kinematic;
    
    //virtual void update( sf::Time delta );
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const = 0;    
    
    virtual void setPosition( const sf::Vector2f &position );
    virtual void setPosition( float x, float y );
    
    bool isBreakable() const;
protected:
    bool m_isBreakable;    
    World *m_world;
    b2Body *m_body;

};

#endif	/* PHYSICENTITY_HPP */

