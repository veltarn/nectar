/* 
 * File:   Sky.cpp
 * Author: Dante
 * 
 * Created on 25 juin 2013, 19:45
 */

#include "Sky.hpp"

using namespace std;

Sky::Sky( map< string, string> skyGradient, const Size mapSize, World *world, bool generateClouds ) : Entity( ),
        m_world( world ),
        m_generateClouds( generateClouds ),
        m_skyTexture( NULL )
{
    calculateGradient( skyGradient, mapSize );    
    initSky( mapSize );
}


Sky::~Sky( )
{
}

void Sky::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    states.transform *= getTransform();
    states.texture = m_skyTexture;
    target.draw( m_vertices, states );
    
}

void Sky::initSky( const Size mapSize)
{
    m_vertices.setPrimitiveType( sf::Quads );
    m_vertices.resize( 4 );
    
    m_vertices[0].position = sf::Vector2f( 0, 0 );
    m_vertices[1].position = sf::Vector2f( mapSize.m_width, 0 );
    m_vertices[2].position = sf::Vector2f( mapSize.m_width, mapSize.m_height );
    m_vertices[3].position = sf::Vector2f( 0, mapSize.m_height );
    
    m_vertices[0].texCoords = sf::Vector2f( 0, 0 );
    m_vertices[1].texCoords = sf::Vector2f( m_skyTexture->getSize().x, 0 );
    m_vertices[2].texCoords = sf::Vector2f( m_skyTexture->getSize().x, m_skyTexture->getSize().y );
    m_vertices[3].texCoords = sf::Vector2f( 0, m_skyTexture->getSize().y );
}

void Sky::calculateGradient( map< string, string > skyGradient, Size mapSize)
{
    vector<int> pixelsPoint;
    sf::Image skyImage;
    skyImage.create( mapSize.m_width, mapSize.m_height );
    
    for( map< string, string >::iterator it = skyGradient.begin(); it != skyGradient.end(); ++it )
    {
        string hexaFormatColor = it->second;
        string strR = hexaFormatColor.substr( 1, 2 );
        string strG = hexaFormatColor.substr( 3, 2 );
        string strB = hexaFormatColor.substr( 5, 2 );
        
        int r = Converter::hexaColorToInt( strR );
        int g = Converter::hexaColorToInt( strG );
        int b = Converter::hexaColorToInt( strB );
        
        sf::Color color( r, g, b );
        m_skyGradient.push_back( color );
        float key = Converter::stringToFloat( it->first );
        int pixelKey = ceil( key * mapSize.m_height );
        pixelsPoint.push_back( pixelKey );
    }
    
    if( pixelsPoint.size() > 1 )
    {
        int startPoint = 0;
        int endPoint = pixelsPoint[1];
        int currentIndexEndPoint = 1;
        
        double stepPerIteration_r = ( (double)m_skyGradient[0].r - (double)m_skyGradient[1].r ) / (double)endPoint;
        double stepPerIteration_g = ( (double)m_skyGradient[0].g - (double)m_skyGradient[1].g ) / (double)endPoint;
        double stepPerIteration_b = ( (double)m_skyGradient[0].b - (double)m_skyGradient[1].b ) / (double)endPoint;
        
        sf::Color currentColor = m_skyGradient[0];
        
        for( int y = 0; y < mapSize.m_height; ++y )
        {
            int currentPoint = y;
            if( currentPoint == endPoint )
            {
                if( currentIndexEndPoint < pixelsPoint.size() - 1 )
                {
                    startPoint = y;
                    endPoint = pixelsPoint[currentIndexEndPoint + 1];
                    currentIndexEndPoint += 1;
                    
                    stepPerIteration_r = ( (double)currentColor.r - (double)m_skyGradient[currentIndexEndPoint].r ) / (double)endPoint;
                    stepPerIteration_g = ( (double)currentColor.g - (double)m_skyGradient[currentIndexEndPoint].g ) / (double)endPoint;
                    stepPerIteration_b = ( (double)currentColor.b - (double)m_skyGradient[currentIndexEndPoint].b ) / (double)endPoint;
                            
                }
            }
            
            sf::Color rowColor( currentColor );
            
            //Crossing each pixel of the row
            for( int x = 0; x < mapSize.m_width; ++x)
            {
                skyImage.setPixel( x, y, rowColor );
            }
            
            currentColor.r -= (sf::Uint8)stepPerIteration_r;
            currentColor.g -= (sf::Uint8)stepPerIteration_g;
            currentColor.b -= (sf::Uint8)stepPerIteration_b;
        }
    }
    else
    {
        sf::Color currentColor = m_skyGradient[0];
        for( int y = 0; y < mapSize.m_height; ++y )
        {
            for( int x = 0; x < mapSize.m_width; ++x )
            {
                skyImage.setPixel( x, y, currentColor );
            }
        }
    }
    
    sf::Texture skyTexture;
    skyTexture.loadFromImage( skyImage );
    
    TextureManager *textureManager = TextureManager::getInstance();
    
    textureManager->add( "sky", skyTexture );
    m_skyTexture = &textureManager->get( "sky" );
}