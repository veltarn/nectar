/* 
 * File:   Renderer.cpp
 * Author: Dante
 * 
 * Created on 17 juin 2013, 23:10
 */

#include "Renderer.hpp"
#include "../System/FruitException.hpp"

using namespace std;

void Renderer::init()
{
    m_log = Log::getInstance();
    *m_log << "Renderer initialized" << endl;
}


Renderer::~Renderer( )
{
    *m_log << "Cleaning render stack" << endl;
    for( vector<DrawableGroup*>::iterator it = m_drawableGroups.begin(); it != m_drawableGroups.end(); ++it )
    {
        delete *it;
    }
    *m_log << "Done" << endl;
    m_drawableGroups.clear();
}

void Renderer::addEntity( std::string groupName, Entity* drawable )
{
    *m_log << "Adding new drawable to group \"" << groupName << "\"" << endl;
    for( int i = 0; i < m_drawableGroups.size(); ++i )
    {
        if( m_drawableGroups.at( i )->getGroupName() == groupName )
        {
            m_drawableGroups.at( i )->push_back( drawable );
        }
    }
}

void Renderer::removeEntity( std::string groupName, Entity* drawable )
{
    *m_log << "Removing drawable into group \"" << groupName << "\"" << endl;
    
    for( int i = 0; m_drawableGroups.size(); ++i )
    {
        if( m_drawableGroups.at( i )->getGroupName() == groupName )
        {
            m_drawableGroups.at( i )->removeDrawable( drawable );
        }
    }
    
}

void Renderer::addDrawableGroup( std::string name )
{    
    //Verifying if name does not exists
    for( int i = 0; i < m_drawableGroups.size(); ++i )
    {
        //If the drawablegroup already exists, we return nothing
        if( m_drawableGroups.at( i )->getGroupName() == name )
        {
            return;
        }
    }
    
    DrawableGroup *dg = new DrawableGroup( name );
    m_drawableGroups.push_back( dg );
}

DrawableGroup *Renderer::getDrawableGroup( std::string name ) const
{
    for( vector<DrawableGroup*>::const_iterator it = m_drawableGroups.begin(); it != m_drawableGroups.end(); ++it )
    {
        if( (*it)->getGroupName() == name )
        {
            return *it;
        }
    }
    
    *m_log << "Cannot find DrawableGroup named \"" << name << "\" returning NULL pointer" << endl;
    return NULL;
}

void Renderer::deleteDrawableGroup( std::string name )
{
    int index = 0;
    bool found = false;
    for( int i = 0; i < m_drawableGroups.size() && !found; ++i )
    {
        if( m_drawableGroups.at( i )->getGroupName() == name )
        {
            index = i;
            found = true;
        }
    }
    
    delete m_drawableGroups[index];
    m_drawableGroups.erase( m_drawableGroups.begin() + index );
}

void Renderer::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    for( vector<DrawableGroup*>::const_iterator it = m_drawableGroups.begin(); it != m_drawableGroups.end(); ++it )
    {
        target.setView( (*it)->getView() );
        target.draw( **it, states );
    }
}

sf::View &Renderer::getView( std::string groupName )
{
    for( vector<DrawableGroup*>::iterator it = m_drawableGroups.begin(); it != m_drawableGroups.end(); ++it )
    {
        if( (*it)->getGroupName() == groupName )
        {
            return (*it)->getView();
        }
    }
    *m_log << "Cannot find \"" << groupName << "\" drawable group, returning invalid view..." << endl;
    throw FruitException( 7, "Cannot find \"" + groupName + "\" drawable group", FruitException::FATAL );
}

void Renderer::clearDrawableGroupContents( std::string drawableGroupName )
{
    for( vector<DrawableGroup*>::iterator it = m_drawableGroups.begin(); it != m_drawableGroups.end(); ++it )
    {
        if( (*it)->getGroupName() == drawableGroupName )
        {
            // @todo delete entities!
            (*it)->clear();
        }
    }
}