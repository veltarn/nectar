/* 
 * File:   Entity.cpp
 * Author: Dante
 * 
 * Created on 12 juin 2013, 19:33
 */

#include "Entity.hpp"


Entity::Entity( ) :  m_toDelete( false )
{
}


Entity::~Entity( )
{
}

/*void Entity::update( sf::Time delta )
{
}*/

    
void Entity::setPosition( const sf::Vector2f &position ) {
    sf::Transformable::setPosition( position );
}

void Entity::setPosition( float x, float y ) {
    sf::Transformable::setPosition( x, y );
}