/* 
 * File:   PhysicEntity.cpp
 * Author: Dante
 * 
 * Created on 25 juin 2013, 20:02
 */

#include "PhysicEntity.hpp"
#include "../Game/World.hpp"

PhysicEntity::PhysicEntity( World *world ) : Entity( ), m_world( world )
{
} 

PhysicEntity::~PhysicEntity( )
{
}

b2BodyType PhysicEntity::getBodyType() const
{    
    return m_body->GetType();
}

void PhysicEntity::setBody( b2Body* body )
{
    m_body = body;
}

b2Body *PhysicEntity::getBody() const
{
    return m_body;
}

/*void PhysicEntity::update( sf::Time delta )
{
    std::cout << getPosition().x << " / " << getPosition().y << std::endl;
    setPosition( m_body->GetPosition().x * m_world->getWorldScale(), m_body->GetPosition().y * m_world->getWorldScale() );    
    std::cout << getPosition().x << " / " << getPosition().y << std::endl;
    setRotation( ( m_body->GetAngle() * 180.f ) / b2_pi );
}*/

bool PhysicEntity::isBreakable() const
{
    return m_isBreakable;
}


void PhysicEntity::setPosition( const sf::Vector2f &position ) {
    Entity::setPosition( position );
}

void PhysicEntity::setPosition( float x, float y ) {
    Entity::setPosition( x, y );
}