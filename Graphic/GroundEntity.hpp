/* 
 * File:   GroundEntity.hpp
 * Author: Dante
 *
 * Created on 13 juin 2013, 02:07
 */

#ifndef GROUNDENTITY_HPP
#define	GROUNDENTITY_HPP

#include "PhysicEntity.hpp"

class World;

class GroundEntity : public PhysicEntity
{
public:
    GroundEntity( World *world );
    virtual ~GroundEntity( );
    
    void setTexture( sf::Texture *texture );
    sf::Texture *getTexture() const;
	
    /**
    *	Mettre à jour SFML ET le moteur physique!
    */
    void setPosition( const sf::Vector2f &position );
    
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
private:
    sf::Texture *m_texture;
    sf::VertexArray m_vertices;
};

#endif	/* GROUNDENTITY_HPP */

