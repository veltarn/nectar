/* 
 * File:   BreakableEntity.hpp
 * Author: Dante
 *
 * Created on 12 juin 2013, 20:20
 */

#ifndef BREAKABLEENTITY_HPP
#define	BREAKABLEENTITY_HPP

#include "Entity.hpp"
#include "PhysicEntity.hpp"

class World;

class BreakableEntity : public PhysicEntity
{
public:
    BreakableEntity( World *world );
    BreakableEntity( unsigned int currentHealth, unsigned int maximumHealth, World *world );
    virtual ~BreakableEntity( );
    
    void setHealth( unsigned int health );
    void decreaseHealth( int amount );
    void increaseHealth( unsigned int amout );
    unsigned int getHealth() const;
    
    void setMaximumHealth( unsigned int maximum );
    unsigned int getMaximumHealth() const;
    
    virtual void update( sf::Time delta );
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const = 0;
protected:
    virtual void onDestroy() = 0;
    
protected:
    unsigned int m_health;
    unsigned int m_maximumHealth;
};

#endif	/* BREAKABLEENTITY_HPP */

