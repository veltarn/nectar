/* 
 * File:   Window.cpp
 * Author: Dante
 * 
 * Created on 29 mars 2013, 23:18
 */

#include "Window.hpp"

Window::Window()
{
    m_log = Log::getInstance();
    *m_log << "Creating default Window without specifics dimensions" << std::endl;
}

Window::Window(sf::VideoMode vm, const std::string &title) : sf::RenderWindow(vm, title)
{
    m_log = Log::getInstance();
    *m_log << "Creating Window with dimensions " << vm.width << "x" << vm.height << std::endl;
}

Window::~Window() {
    *m_log << "Window destroyed";
}

sf::FloatRect Window::getViewportPosition()
{
    sf::View v = this->getView();

    return getViewportPosition(v);
}

sf::FloatRect Window::getViewportPosition(sf::View view)
{
    sf::FloatRect viewportPos;
    viewportPos.left = view.getCenter().x - (this->getSize().x / 2);
    viewportPos.width = this->getSize().x;
    viewportPos.top = view.getCenter().y - (this->getSize().y / 2);
    viewportPos.height = this->getSize().y;

    return viewportPos;
}

void Window::draw(const sf::Drawable &drawable, const sf::RenderStates &states)
{
    sf::RenderWindow::draw(drawable, states);
}