/* 
 * File:   ImageEntity.cpp
 * Author: shen
 * 
 * Created on 23 septembre 2013, 17:00
 */

#include "ImageEntity.hpp"

ImageEntity::ImageEntity( std::string fileName ) : Entity() 
{
    m_entityTexture = new sf::Texture();
    if( !m_entityTexture->loadFromFile( fileName ) )
    {
        delete m_entityTexture;
        m_entityTexture = nullptr;
        throw FruitException( 11, "Cannot open image file " + fileName + " in ImageEntity.cpp", FruitException::CRITICAL );
    }
    
    m_vertices.setPrimitiveType( sf::Quads );
    m_vertices.resize( 4 );
    
    sf::Vector2u textSize = m_entityTexture->getSize();
    
    m_vertices[0].position = sf::Vector2f( getPosition().x, getPosition().y );
    m_vertices[1].position = sf::Vector2f( getPosition().x + textSize.x, getPosition().y );
    m_vertices[2].position = sf::Vector2f( getPosition().x + textSize.x, getPosition().y + textSize.y );
    m_vertices[3].position = sf::Vector2f( getPosition().x, getPosition().y + textSize.y );
    
    m_vertices[0].texCoords = sf::Vector2f( 0, 0 );
    m_vertices[1].texCoords = sf::Vector2f( textSize.x, 0 );
    m_vertices[2].texCoords = sf::Vector2f( textSize.x, textSize.y );
    m_vertices[3].texCoords = sf::Vector2f( 0, textSize.y );
    
    setOrigin( getPosition().x, getPosition().y );
}

/*ImageEntity::ImageEntity(const ImageEntity& orig) {
}*/

ImageEntity::~ImageEntity() {
}

void ImageEntity::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    states.transform *= getTransform();
    states.texture = m_entityTexture;
    target.draw( m_vertices, states );
}
