/* 
 * File:   DrawableGroup.cpp
 * Author: Dante
 * 
 * Created on 20 février 2013, 17:00
 */

#include "DrawableGroup.hpp"

using namespace std;

DrawableGroup::DrawableGroup( std::string name ) : m_groupName( name )
{
}


DrawableGroup::~DrawableGroup() {
}

std::string DrawableGroup::getGroupName() const
{
    return m_groupName;
}

void DrawableGroup::draw( sf::RenderTarget &target, sf::RenderStates states ) const
{
    //cout << "Drawing " << getGroupName() << endl;
    for(std::vector<Entity*>::const_iterator it = begin(); it != end(); ++it){
        target.draw(**it, states);
    }
}

void DrawableGroup::removeDrawable( Entity *drawable )
{
    bool found;
    int index = 0;
    for( int i = 0; i < size() && !found; ++i )
    {
        if( this->at( i ) == drawable )
        {
            found = true;
            index = i;
        }
    }
    
    this->erase( this->begin() + index );
}

sf::View &DrawableGroup::getView()
{
    return m_drawableGroupView;
}

void DrawableGroup::setView( sf::View view )
{
    m_drawableGroupView = view;
}