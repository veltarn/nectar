/* 
 * File:   DrawableGroup.h
 * Author: Dante
 *
 * Created on 20 février 2013, 17:00
 */
#ifndef DRAWABLEGROUP_H
#define	DRAWABLEGROUP_H

#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "Entity.hpp"

class Entity;
class DrawableGroup : public sf::Drawable, public std::vector<Entity *>
{
public:
    DrawableGroup( std::string name );
    virtual ~DrawableGroup();

    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
    
    std::string getGroupName() const;
    void removeDrawable( Entity *drawable );
    
    sf::View &getView();
    void setView( sf::View view );
private:
    std::string m_groupName;
    sf::View m_drawableGroupView;

};
#endif	/* DRAWABLEGROUP_H */

