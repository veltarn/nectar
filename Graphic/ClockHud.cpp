/* 
 * File:   ClockHud.cpp
 * Author: Dante
 * 
 * Created on 3 juillet 2013, 15:25
 */

#include "ClockHud.hpp"

ClockHud::ClockHud( const FrameClock *clock, const sf::Font &font ) : m_clock( clock ), m_font( font ), m_hidden( false )
{
}

void ClockHud::draw( sf::RenderTarget& target, sf::RenderStates states ) const
{
    if( !m_hidden )
    {
        const Stats stats = build();

        sf::Text elem;
        elem.setFont( m_font );
        elem.setCharacterSize( 16 );
        elem.setPosition( 5.0f, 5.0f );

        //Draw every frame statistics
        for( std::size_t i = 0; i < stats.size(); ++i )
        {
            elem.setString( stats[i].str );
            elem.setColor( stats[i].color );

            target.draw( elem, states );

            elem.move( 0.0f, 16.0f );
        }
    }
}

ClockHud::Stats ClockHud::build() const
{
    const Stat stats[10] = {
        { sf::Color::Yellow, format( "Time", "(sec)", m_clock->getTotalFrameTime().asSeconds() )        },
        { sf::Color::White, format( "Frame", "", m_clock->getTotalFrameCount() )                        },
        { sf::Color::Green, format( "FPS", "", m_clock->getFramesPerSecond() )                          },
        { sf::Color::Green, format( "min", "", m_clock->getMinFramesPerSecond() )                       },
        { sf::Color::Green, format( "avg", "", m_clock->getAverageFramesPerSecond() )                   },
        { sf::Color::Green, format( "max", "", m_clock->getMaxFramesPerSecond() )                       },
        { sf::Color::Cyan, format( "Delta", "(ms)", m_clock->getLastFrameTime().asMilliseconds() )      },
        { sf::Color::Cyan, format( "min", "(ms)", m_clock->getMinFrameTime().asMilliseconds() )         },
        { sf::Color::Cyan, format( "avg", "(ms)", m_clock->getAverageFrameTime().asMilliseconds() )     },
        { sf::Color::Cyan, format( "max", "(ms)", m_clock->getMaxFrameTime().asMilliseconds() )         }
    };
    
    return Stats( &stats[0], &stats[10] );
}

void ClockHud::show()
{
    m_hidden = false;
}

void ClockHud::hide()
{
    m_hidden = true;
}

bool ClockHud::isHidden() const
{
    return m_hidden;
}