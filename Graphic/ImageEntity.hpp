/* 
 * File:   ImageEntity.hpp
 * Author: shen
 *
 * Created on 23 septembre 2013, 17:00
 */

#ifndef IMAGEENTITY_HPP
#define	IMAGEENTITY_HPP

#include "Entity.hpp"
#include "../System/FruitException.hpp"

class ImageEntity : public Entity {
public:
    ImageEntity( std::string fileName );
    //ImageEntity(const ImageEntity& orig);
    virtual ~ImageEntity();
    
    virtual void draw( sf::RenderTarget &target, sf::RenderStates states ) const;
private:
    sf::Texture *m_entityTexture;
    sf::VertexArray m_vertices;
};

#endif	/* IMAGEENTITY_HPP */

