/* 
 * File:   PhysFs.hpp
 * Author: shen
 *
 * Created on 5 novembre 2013, 16:45
 */

#ifndef PHYSFS_HPP
#define	PHYSFS_HPP

#include <iostream>
#include <vector>
#include <physfs.h>
#include "singleton.hpp"
#include "Log.hpp"
#include "typenames.hpp"

namespace PhysFs {
    
    enum mode {
        READ, WRITE, APPEND
    };

    typedef PHYSFS_uint8 uint8;
    typedef PHYSFS_sint8 sint8;

    typedef PHYSFS_uint16 uint16;
    typedef PHYSFS_sint16 sint16;

    typedef PHYSFS_uint32 uint32;
    typedef PHYSFS_sint32 sint32;

    typedef PHYSFS_uint64 uint64;
    typedef PHYSFS_sint64 sint64;

    typedef PHYSFS_StringCallback StringCallback;
    typedef PHYSFS_EnumFilesCallback EnumFilesCallback;

    typedef PHYSFS_Version PhysFsVersion;

    typedef PHYSFS_Allocator PhysFsAllocator;

    typedef PHYSFS_ArchiveInfo ArchiveInfo;

    typedef std::vector<ArchiveInfo> ArchiveInfoList;

    /**
     *  @class BaseFStream
     *  @brief Base class of PhysFs file reading in C++
     */
    class BaseFStream {
        private:
            bool m_isOpen;
        protected:
            PHYSFS_File *m_file;
        public:
            BaseFStream( PHYSFS_File *file );
            virtual ~BaseFStream();
            bool eof();
            uint64 length();
            bool isOpen();
            void close();
    };
    
    /**
     *  @class IfStream
     *  @brief Reading class inheriting BaseFStream
     */
    class IfStream : public BaseFStream, public std::istream {
        public:
            IfStream( std::string &filename );
            virtual ~IfStream();
    };
    
    /**
     * @class OfStream
     * @brief Writing class inheriting BaseFStream
     */
    class  OfStream : public BaseFStream, public std::ostream {
        public:
            OfStream( std::string &filename );
            virtual ~OfStream();
    };
    
    /**
     * @class FStream
     */
    class FStream : public BaseFStream, public std::iostream {
        public:
            FStream( std::string filename, mode openMode = READ );
            virtual ~FStream();
    };
    
    /**
     * @class PhysFS
     * @brief Wrapper class of PhysFS C library
     */
    class PhysFs : public Singleton<PhysFs> {
        public:
        
        /**
         * @brief Global initialisation of PHYSFS
         * @param argument of the program
         */
        void init( char const *argv0 );
        
        /**
         * @brief Global shutdown of PHYSFS
         */
        void deinit();
        
        /**
         * @brief Returns true if PHYSFS is initialised, false otherwise
         * @return Returns true if PHYSFS is initialised, false otherwise
         */
        bool isInitialized();

        /**
         * @brief Get version of PHYSFS
         * @return Structure that contains PHYSFS's version
         */
        PhysFsVersion getPhysFsVersion();
        
        /**
         * @brief Get all supported archive type by PHYSFS
         * @return List of ArchiveInfo structure that contains supported archives
         */
        ArchiveInfoList getSupportedArchiveTypes();

        /**
         * @brief Get last human readable error
         * @return String of the last error
         */
        std::string getLastError();

        /**
         * @brief Get the system-dependent dir separator "/" for linux, "\\" for windows...
         * @return String of the separator
         */
        std::string getDirSeparator();

        /**
         * @brief Set PHYSFS that allows or not Symbolic links
         * @param allow
         */
        void allowSymbolicLinks( const bool allow );
        
        /**
         * @brief Get a list of all CDRoms directories
         * The dirs returned are platform-dependent ("D:\" on Win32, "/cdrom" or whatnot on Unix). Dirs are only returned if there is a disc ready and accessible in the drive. So if you've got two drives (D: and E:), and only E: has a disc in it, then that's all you get. If the user inserts a disc in D: and you call this function again, you get both drives. If, on a Unix box, the user unmounts a disc and remounts it elsewhere, the next call to this function will reflect that change.
         * 
         * This function refers to "CD-ROM" media, but it really means "inserted disc media," such as DVD-ROM, HD-DVD, CDRW, and Blu-Ray discs. It looks for filesystems, and as such won't report an audio CD, unless there's a mounted filesystem track on it.
         * @return List of CDRoms directories
         */
        StringList getCdromDirs();

        /**
         * @brief Get the path where the application resides
         * @return String of base directory in plateform-dependant notation
         */
        std::string getBaseDir();
        
        /**
         * @brief Get the path where the user's home resides
         * @return String of the user directory in plateform-dependent notation
         */
        std::string getUserDir();
        
        /**
         * @brief Get path where PhysFs will allow file writing
         * @return String of the path
         */
        std::string getWriteDir();
        
        /**
         * @brief Tells PhysFs where it may write files
         * @param newDir Directory where PhysFs is allow to write
         * @return true if nothing breaks,  false otherwise
         */
        bool setWriteDir( std::string newDir );

        /**
         * @brief Adds newDir to search path.
         * This is a legacy call in PhysicsFS 2.0, equivalent to: PHYSFS_mount(newDir, NULL, appendToPath);
         * 
         * You must use this and not PHYSFS_mount if binary compatibility with PhysicsFS 1.0 is important (which it may not be for many people).
         * @deprecated This method will be replaced by mount(std::string newDir, std::string virtualPath, bool append )
         * @param newDir Directory which be used by PhysFS to search for files and archives
         * @param append
         * @return 
         */
        bool addToSearchPath( std::string newDir, bool append );
        
        /**
         * @brief Remove a directory or archive from the search path
         * This must be a (case-sensitive) match to a dir or archive already in the search path, specified in platform-dependent notation.
         * 
         * This call will fail (and fail to remove from the path) if the element still has files open in it.
         * @param dir Dir / archive to remove
         * @return False if something went wrong, true otherwise
         */
        bool removeFromSearchPath( std::string dir );
        
        /**
         * @brief Add an archive or directory to the search path
         * If this is a duplicate, the entry is not added again, even though the function succeeds. You may not add the same archive to two different mountpoints: duplicate checking is done against the archive and not the mountpoint.
         * 
         * When you mount an archive, it is added to a virtual file system...all files in all of the archives are interpolated into a single hierachical file tree. Two archives mounted at the same place (or an archive with files overlapping another mountpoint) may have overlapping files: in such a case, the file earliest in the search path is selected, and the other files are inaccessible to the application. This allows archives to be used to override previous revisions; you can use the mounting mechanism to place archives at a specific point in the file tree and prevent overlap; this is useful for downloadable mods that might trample over application data or each other, for example.
         *
         * The mountpoint does not need to exist prior to mounting, which is different than those familiar with the Unix concept of "mounting" may not expect. As well, more than one archive can be mounted to the same mountpoint, or mountpoints and archive contents can overlap...the interpolation mechanism still functions as usual.
         * @param newDir Directory or archive to add to the path, in plateforme-dependant notation.
         * @param virtualPath Location in the virtual tree. (In plateform-dependant notation)
         * @param append true: Append to search path, false to prepend to search path
         * @return true if all went good, false otherwise
         */
        bool mount( std::string newDir, std::string virtualPath, bool append );
        
        /**
         * @brief Add and archive or directory to the search path and mount them into a temporary virtual location
         * @param newDir Directory or archive to add to the path, in plateforme-dependant notation.
         * @param subPath Location of the path into the temporary location (eg: "subFolder/")
         * @param append true: Append to search path, false to prepend to search path
         * @return true if everything went good, false otherwise
         */
        bool mountTmp( std::string newDir, std::string subPath, bool append );
        
        /**
         * @brief Get the mount point of a mounted archive / dir
         * You give this function the name of an archive or dir you successfully added to the search path, and it reports the location in the interpolated tree where it is mounted. Files mounted with a NULL mountpoint or through PHYSFS_addToSearchPath() will report "/". The return value is READ ONLY and valid until the archive is removed from the search path.
         * @param dir Directory of archive added to the path.
         * @return String of the mounted point
         */
        std::string getMountPoint( std::string dir );

        /**
         * @brief Set up sane, default paths. 
         * The write dir will be set to "userdir/.organization/appName", which is created if it doesn't exist.

         * The above is sufficient to make sure your program's configuration directory is separated from other clutter, and platform-independent. The period before "mygame" even hides the directory on Unix systems.
         * 
         * The search path will be:
         * 
         * + The Write Dir (created if it doesn't exist)
         * + The Base Dir (PHYSFS_getBaseDir())
         * + All found CD-ROM dirs (optionally)
         * 
         * These directories are then searched for files ending with the extension (archiveExt), which, if they are valid and supported archives, will also be added to the search path. If you specified "PKG" for (archiveExt), and there's a file named data.PKG in the base dir, it'll be checked. Archives can either be appended or prepended to the search path in alphabetical order, regardless of which directories they were found in.
         * 
         * All of this can be accomplished from the application, but this just does it all for you. Feel free to add more to the search path manually, too.
         * 
         * @param organisationName Name of your company / group etc to be used as dirname
         * @param appName Name of the program
         * @param archiveExtension File extension used to specify and archive. (eg: For .zip extension, write "zip" )
         * @param archivesFirst Prepend archive in search path if *true*, if *false*, append archives to the search path.
         * @param includeCdRoms Include cdroms directories if *true*, doesn't if false
         * @return return true if everything went good, false otherwise
         */
        bool setSaneConfig( std::string organisationName, std::string appName, std::string archiveExtension, bool archivesFirst = true, bool includeCdRoms = false );

        /**
         * @brief Create a new folder
         * @param dirname Name of the folder
         * @return true if everything went good, false otherwise
         */
        bool mkdir( std::string dirname );

        /**
         * @brief Removes a file or directory
         * @param filename Name of the file or directory
         * @return true if everything went good, false otherwise
         */
        bool remove( std::string filename );

        /**
         * @brief Figure ouet where in the search path a file resides
         * The file is specified in platform-independent notation. The returned filename will be the element of the search path where the file was found, which may be a directory, or an archive. Even if there are multiple matches in different parts of the search path, only the first one found is used, just like when opening a file.
         * 
         * So, if you look for "maps/level1.map", and C:\mygame is in your search path and C:\mygame\maps\level1.map exists, then "C:\mygame" is returned.
         * 
         * If a any part of a match is a symbolic link, and you've not explicitly permitted symlinks, then it will be ignored, and the search for a match will continue.
         * 
         * If you specify a fake directory that only exists as a mount point, it'll be associated with the first archive mounted there, even though that directory isn't necessarily contained in a real archive.
         * @param filename File to look for
         * @return string of element of search path containing the the file in question
         */
        std::string getRealDir( std::string filename );

        /**
         * @brief Get the current search path
         * @return Array of string representing the paths
         */
        StringList getSearchPath();
        
        /**
         * @brief Get a listing of a search path's directory
         * Matching directories are interpolated. That is, if "C:\mydir" is in the search path and contains a directory "savegames" that contains "x.sav", "y.sav", and "z.sav", and there is also a "C:\userdir" in the search path that has a "savegames" subdirectory with "w.sav", then the following code:
         * @param dir Directory in plateform-dependant notation
         * @return Array of string representing directories
         */
        StringList enumerateFiles( std::string dir );
        
        /**
         * @brief Determines if a file exists
         * @param filename File
         * @return true if the file exists, false otherwise
         */
        bool exists( std::string filename );
        
        /**
         * @brief Determines if a file in the search path is a directory
         * @param filename File
         * @return true if it's a directory, false otherwise
         */
        bool isDirectory( std::string filename );
        
        /**
         * @brief Determines if a file is a symbolic link
         * @param filename
         * @return true if it's a symbolic link, false otherwise
         */
        bool isSymbolicLink( std::string filename );
        
        /**
         * @brief Get the timestamp of the last modification of the file
         * @param filename
         * @return Timestamp of the file the last time it was modified
         */
        sint64 getLastModTime( std::string filename );
        
        /**
         * @brief Opens a PhysFs file in -openMode- mode
         * @param filename File to be opened
         * @param openMode WRITE, READ or APPEND @see enum mode
         * @return Pointer to the PHYSFS_File structure
         */
        static PHYSFS_File *open( std::string filename, mode openMode );
        
                


    private:
        Log *m_log;

    };

}

#endif	/* PHYSFS_HPP */

