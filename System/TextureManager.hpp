/* 
 * File:   ResourcesManager.hpp
 * Author: Dante
 *
 * Created on 13 juin 2013, 16:53
 */

#ifndef RESOURCESMANAGER_HPP
#define	RESOURCESMANAGER_HPP

#include <iostream>
#include <map>
#include <SFML/Graphics.hpp>
#include "Log.hpp"
#include "FruitException.hpp"
#include "singleton.hpp"

typedef typename std::map<std::string, sf::Texture>::iterator TextureIterator;
class TextureManager : public Singleton<TextureManager>
{
    friend class Singleton<TextureManager>;
public:
    void add( std::string name, sf::Texture texture );
    void load( std::string path );
    sf::Texture &get( std::string path );
    void remove( std::string path );
    bool exists( std::string path );
    void clear();
protected:
    ~TextureManager();
private:    
    std::map<std::string, sf::Texture> m_resourcesList;
};

#endif	/* RESOURCESMANAGER_HPP */

