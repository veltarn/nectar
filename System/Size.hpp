/* 
 * File:   Size.hpp
 * Author: Dante
 *
 * Created on 8 juin 2013, 19:10
 */

#ifndef SIZE_HPP
#define	SIZE_HPP

#include <iostream>


class Size {
public:
    Size();
    Size(float width, float height);
    Size(const Size& orig);
    virtual ~Size();

    void display( std::ostream &flux ) const;

    void setSize(float width, float height);

    void setWidth(float width);
    float getWidth() const;
    void setHeight(float height);
    float getHeight() const;

    Size operator+( Size const& sizeB );
    Size operator-( Size const& sizeB );
    Size operator*( Size const& sizeB );
    Size operator/( Size const& sizeB );

    Size& operator+=( const Size &other );
    Size& operator-=( const Size &other );
    Size& operator*=( const Size &other );
    Size& operator/=( const Size &other );

    Size &operator=( const Size & other );


    bool operator==( Size const& b );
    bool operator!=( Size const& b);
    bool operator<( Size const& b);
    bool operator>( Size const& b);
    bool operator<=( Size const& b);
    bool operator>=( Size const& b);
    
public:    
    float m_width;
    float m_height;

};
/*Size operator+( Size const& sizeA, Size const& sizeB );
Size operator-( Size const& sizeA, Size const& sizeB );
Size operator*( Size const& sizeA, Size const& sizeB );
Size operator/( Size const& sizeA, Size const& sizeB );*/

std::ostream &operator<<( std::ostream &stream, Size const& size );



#endif	/* SIZE_HPP */

