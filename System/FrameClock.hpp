////////////////////////////////////////////////////////////
//
// FrameClock - Utility class for tracking frame statistics.
// Copyright (C) 2013 Lee R (lee-r@outlook.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
//    you must not claim that you wrote the original software.
//    If you use this software in a product, an acknowledgment
//    in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
//    and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#ifndef FRAMECLOCK_H_INCLUDED
#define FRAMECLOCK_H_INCLUDED

#include <limits>
#include <vector>
#include <cassert>
#include <algorithm>

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>

namespace detail
{
    // Utility function to disambiguate from macro 'max'.
    template<typename T>
    inline T limit()
    {
        return (std::numeric_limits<T>::max)();
    }
}	// namespace detail

class FrameClock
{
public:

    // Constructs a FrameClock object with sample depth 'depth'.
    explicit FrameClock( std::size_t depth = 100 );

    // Resets all times to zero and discards accumulated samples.
    void clear();

    // Begin frame timing.
    // Should be called once at the start of each new frame.
    void beginFrame();

    // End frame timing.
    // Should be called once at the end of each frame.
    // Returns: Time elapsed since the matching FrameClock::beginFrame.
    sf::Time endFrame();

    // Sets the number of frames to be sampled for averaging.
    // 'depth' must be greater than or equal to 1.
    void setSampleDepth(std::size_t depth);

    // Returns: The number of frames to be sampled for averaging.
    std::size_t getSampleDepth() const;

    // Returns: The total accumulated frame time.
    sf::Time getTotalFrameTime() const;

    // Returns: The total accumulated number of frames.
    sf::Uint64 getTotalFrameCount() const;

    // Returns: Time elapsed during the last 'FrameClock::beginFrame/endFrame' pair.
    sf::Time getLastFrameTime() const;

    // Returns: The shortest measured frame time.
    sf::Time getMinFrameTime() const;

    // Returns: The longest measured frame time.
    sf::Time getMaxFrameTime() const;

    // Returns: Average frame time over the last getSampleDepth() frames.
    sf::Time getAverageFrameTime() const;

    // Returns: Frames per second, considering the pervious frame only.
    float getFramesPerSecond() const;

    // Returns: The lowest measured frames per second.
    float getMinFramesPerSecond() const;

    // Returns: The highest measured frames per second.
    float getMaxFramesPerSecond() const;

    // Returns: Average frames per second over the last getSampleDepth() frames.
    float getAverageFramesPerSecond() const;

    // Swaps the value of this FrameClock instance with 'other'.
    void swap(FrameClock& other);

private:

    template<typename T, typename U>
    struct Range
    {
        Range() : minimum(), maximum(), average(), current(), elapsed()
        {}
        void swap( Range& other )
        {
            std::swap( this->minimum, other.minimum );
            std::swap( this->maximum, other.maximum );
            std::swap( this->average, other.average );
            std::swap( this->current, other.current );
            std::swap( this->elapsed, other.elapsed );
        }
        T minimum;
        T maximum;
        T average;
        T current;
        U elapsed;
    };

    Range<sf::Time, sf::Time> m_time;
    Range<float, sf::Uint64>  m_freq;

    struct SampleData
    {
        SampleData() : accumulator(), data(), index()
        {}
        
        sf::Time accumulator;
        std::vector<sf::Time> data;
        std::vector<sf::Time>::size_type index;
        void swap( SampleData& other )
        {
            std::swap( this->accumulator, other.accumulator );
            std::swap( this->data,        other.data );
            std::swap( this->index,       other.index );
        }
    } m_sample;

    sf::Clock m_clock;
};

#endif	// FRAMECLOCK_H_INCLUDED