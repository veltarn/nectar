/* 
 * File:   Dir.cpp
 * Author: Dante
 * 
 * Created on 20 février 2013, 17:54
 */

#include <boost/filesystem/operations.hpp>

#include "Dir.hpp"


using namespace std;
using namespace boost::filesystem;


Dir::Dir(const char *rootPath)
{
    m_rootPath = path(rootPath);
}

Dir::Dir(string rootPath)
{
    m_rootPath = path(rootPath.c_str());
}

Dir::Dir(const Dir& orig) {
    m_rootPath = orig.m_rootPath;
}

Dir::~Dir() {
}


bool Dir::exists( std::string path ) {
    return boost::filesystem::exists( path );
}

vector<string> Dir::entryList( Dir::Filter filter, vector<string> extensionsFilter )
{
    vector<path> v;
    vector<string> entriesList;
    copy(directory_iterator(m_rootPath), directory_iterator(), back_inserter(v));

    for(vector<path>::iterator it(v.begin()); it != v.end(); ++it)
    {
        if( filter == Dir::FILES )
        {
            if( is_regular_file( *it ) )
            {
                if( allowedExtension( *it , extensionsFilter ) )
                    entriesList.push_back( it->filename().string() );
            }
        }
        else if( filter == Dir::DIRECTORIES )
        {
            if( is_directory( *it ) )
            {
                entriesList.push_back( it->filename().string() );
            }
        }
        else
        {
                entriesList.push_back( it->filename().string() );
        }
    }

    return entriesList;
}

std::string Dir::absoluteFilePath()
{
    return m_rootPath.string();
}

bool Dir::allowedExtension( boost::filesystem::path &filename, vector<string> referenceExtension )
{
    if( referenceExtension.size() == 0 )
    {
        return true;
    }
    else
    {
        for( vector<string>::iterator it = referenceExtension.begin(); it != referenceExtension.end(); ++it )
        {
            if( filename.extension().string() == *it )
            {
                return true;
            }
        }
        return false;
    }
    
    return false;
}