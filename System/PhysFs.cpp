/* 
 * File:   PhysFs.cpp
 * Author: shen
 * 
 * Created on 5 novembre 2013, 16:45
 */

#include <streambuf>
#include "PhysFs.hpp"

/**
 Inherits streambuf
 */

using namespace std;

class fbuf : public streambuf {
private:
        fbuf(const fbuf & other);
        fbuf& operator=(const fbuf& other);

        int_type underflow() {
                if (PHYSFS_eof(file)) {
                        return traits_type::eof();
                }
                size_t bytesRead = PHYSFS_read(file, buffer, 1, bufferSize);
                if (bytesRead < 1) {
                        return traits_type::eof();
                }
                setg(buffer, buffer, buffer + bytesRead);
                return (unsigned char) *gptr();
        }

        pos_type seekoff(off_type pos, ios_base::seekdir dir, ios_base::openmode mode) {
                switch (dir) {
                case std::ios_base::beg:
                        PHYSFS_seek(file, pos);
                        break;
                case std::ios_base::cur:
                        // subtract characters currently in buffer from seek position
                        PHYSFS_seek(file, (PHYSFS_tell(file) + pos) - (egptr() - gptr()));
                        break;
                case std::ios_base::end:
                        PHYSFS_seek(file, PHYSFS_fileLength(file) + pos);
                        break;
                }
                if (mode & std::ios_base::in) {
                        setg(egptr(), egptr(), egptr());
                }
                if (mode & std::ios_base::out) {
                        setp(buffer, buffer);
                }
                return PHYSFS_tell(file);
        }

        pos_type seekpos(pos_type pos, std::ios_base::openmode mode) {
                PHYSFS_seek(file, pos);
                if (mode & std::ios_base::in) {
                        setg(egptr(), egptr(), egptr());
                }
                if (mode & std::ios_base::out) {
                        setp(buffer, buffer);
                }
                return PHYSFS_tell(file);
        }

        int_type overflow( int_type c = traits_type::eof() ) {
                if (pptr() == pbase() && c == traits_type::eof()) {
                        return 0; // no-op
                }
                if (PHYSFS_write(file, pbase(), pptr() - pbase(), 1) < 1) {
                        return traits_type::eof();
                }
                if (c != traits_type::eof()) {
                        if (PHYSFS_write(file, &c, 1, 1) < 1) {
                                return traits_type::eof();
                        }
                }

                return 0;
        }

        int sync() {
                return overflow();
        }

        char * buffer;
        size_t const bufferSize;
protected:
        PHYSFS_File * const file;
public:
        fbuf(PHYSFS_File * file, std::size_t bufferSize = 2048) : file(file), bufferSize(bufferSize) {
                buffer = new char[bufferSize];
                char * end = buffer + bufferSize;
                setg(end, end, end);
                setp(buffer, end);
        }

        ~fbuf() {
                sync();
                delete [] buffer;
        }
};
/**
 End of inherit
 */

/**
 * BaseFStream
 */
PhysFs::BaseFStream::BaseFStream(PHYSFS_File* file) : m_file( file ) {
    if( !file )
        m_isOpen = false;
    else
        m_isOpen = true;
}

PhysFs::BaseFStream::~BaseFStream() {
    PHYSFS_close( m_file );
}

bool PhysFs::BaseFStream::eof() {
    return PHYSFS_eof( m_file );
}

PhysFs::uint64 PhysFs::BaseFStream::length() {
    return PHYSFS_fileLength( m_file );
}

bool PhysFs::BaseFStream::isOpen() {
    return m_isOpen;
}

void PhysFs::BaseFStream::close() {
    if( m_isOpen )
    {
        PHYSFS_close( m_file );
        m_file = nullptr;
    }
}

/**
 *      IfStream
 */

PhysFs::IfStream::IfStream( string& filename ) : BaseFStream( PhysFs::open( filename, mode::READ ) ), istream( new fbuf( m_file ) ) {

}

PhysFs::IfStream::~IfStream() {
    delete rdbuf();
}

/**
 *      OfStream
 */

PhysFs::OfStream::OfStream( string& filename ) : BaseFStream( PhysFs::open( filename, mode::WRITE ) ), ostream( new fbuf( m_file ) ) {
    
}

PhysFs::OfStream::~OfStream() {
    delete rdbuf();
}

/**
 * FStream
 */

PhysFs::FStream::FStream( string filename, mode openMode) : BaseFStream( PhysFs::open( filename, openMode ) ), iostream( new fbuf( m_file ) ) {
    
}

PhysFs::FStream::~FStream() {
    delete rdbuf();
}

/**
 *      PhysFs Class wrapper
 */

void PhysFs::PhysFs::init( char const *argv0 ) {
    PHYSFS_init( argv0 );
    m_log = Log::getInstance();
}


void PhysFs::PhysFs::deinit() {
    PHYSFS_deinit();
}

bool PhysFs::PhysFs::isInitialized() {
    int isInit = PHYSFS_isInit();
    bool ok = isInit != 0 ? true : false;
    return ok;
}

PhysFs::PhysFsVersion PhysFs::PhysFs::getPhysFsVersion() {
    PhysFsVersion version;
    PHYSFS_getLinkedVersion( &version );
    return version;
}

PhysFs::ArchiveInfoList PhysFs::PhysFs::getSupportedArchiveTypes() {
    ArchiveInfoList list;
    
    for( const ArchiveInfo ** archiveType = PHYSFS_supportedArchiveTypes(); *archiveType != NULL; archiveType++ ) {
        list.push_back( **archiveType );
    }
    return list;
}

string PhysFs::PhysFs::getLastError() {
    const char *error = PHYSFS_getLastError();
    if( error != NULL )
        return error;
    else
        return string( "" );
}

string PhysFs::PhysFs::getDirSeparator() {
    const char *separator = PHYSFS_getDirSeparator();
    if( separator != NULL ) {
        return separator;
    } else {
        return string( "" );
    }
}

void PhysFs::PhysFs::allowSymbolicLinks( const bool allow ) {
    PHYSFS_permitSymbolicLinks( allow );
}
StringList PhysFs::PhysFs::getCdromDirs() {
    StringList list;
    
    char **cdromDirs = PHYSFS_getCdRomDirs();
    for( char **c = cdromDirs; *c != NULL; c++ ) {
        list.push_back( *c );
    }
    PHYSFS_freeList( cdromDirs );
    return list;
}

string PhysFs::PhysFs::getBaseDir() {
    const char *dir = PHYSFS_getBaseDir();
    if( dir != NULL ) {
        return dir;
    } else {
        return string( "" );
    }
}

string PhysFs::PhysFs::getUserDir() {
    const char *dir = PHYSFS_getUserDir();
    if( dir != NULL ) {
        return dir;
    } else {
        return string( "" );
    }
}

string PhysFs::PhysFs::getWriteDir() {
    const char *dir = PHYSFS_getWriteDir();
    if( dir != NULL ) {
        return dir;
    } else {
        return string( "" );
    }
}

bool PhysFs::PhysFs::setWriteDir( std::string newDir ) {
    PHYSFS_setWriteDir( newDir.c_str() );
}

bool PhysFs::PhysFs::addToSearchPath( std::string newDir, bool append ) {
    return PHYSFS_addToSearchPath( newDir.c_str(), 1 );
}

bool PhysFs::PhysFs::removeFromSearchPath( std::string dir ) {
    return PHYSFS_removeFromSearchPath( dir.c_str() );
}

bool PhysFs::PhysFs::mount( std::string newDir, std::string virtualPath, bool append ) {
    return PHYSFS_mount( newDir.c_str(), virtualPath.c_str(), append );
}

bool PhysFs::PhysFs::mountTmp( string newDir, string subPath, bool append ) {
    string tempPath = "/temp";
    if( subPath.substr( 0, 1 ) != "/" ) {
        subPath = "/" + subPath;
    }
    string fullPath = tempPath + subPath;
    return this->mount( newDir, fullPath, append );
}

string PhysFs::PhysFs::getMountPoint( std::string dir ) {
    const char *point = PHYSFS_getMountPoint( dir.c_str() );
    
    if( point != NULL ) {
        return point;
    } else {
        return string( "" );
    }
}

bool PhysFs::PhysFs::setSaneConfig( std::string organisationName, std::string appName, std::string archiveExtension, bool archivesFirst, bool includeCdRoms ) {
    return PHYSFS_setSaneConfig( organisationName.c_str(), appName.c_str(), archiveExtension.c_str(), includeCdRoms, archivesFirst );    
}

bool PhysFs::PhysFs::mkdir( std::string dirname ) {
    return PHYSFS_mkdir( dirname.c_str() );
}

bool PhysFs::PhysFs::remove( std::string filename ) {
    return PHYSFS_delete( filename.c_str() );
}

string PhysFs::PhysFs::getRealDir( std::string filename ) {
    const char *dir = PHYSFS_getRealDir( filename.c_str() );
    
    if( dir != NULL ) {
        return dir;
    } else {
        return string( "" );
    }
}

StringList PhysFs::PhysFs::getSearchPath() {
    StringList pathList;
    char **sPath = PHYSFS_getSearchPath();
    for( char **path = sPath; *path != NULL; path++ ) {
        pathList.push_back( *path );
    }
    PHYSFS_freeList( sPath );
    return pathList;
}

StringList PhysFs::PhysFs::enumerateFiles( std::string dir ) {
    StringList files;
    char **fileList = PHYSFS_enumerateFiles( dir.c_str() );
    
    for( char **file = fileList; *file != NULL; file++ ) {
        files.push_back( *file );
    }
    PHYSFS_freeList( fileList );
    return files;
}

bool PhysFs::PhysFs::exists( std::string filename ) {
    return PHYSFS_exists( filename.c_str() );
}

bool PhysFs::PhysFs::isDirectory( std::string filename ) {
    return PHYSFS_isDirectory( filename.c_str() );
}

bool PhysFs::PhysFs::isSymbolicLink( std::string filename ) {
    return PHYSFS_isSymbolicLink( filename.c_str() );
}

PhysFs::sint64 PhysFs::PhysFs::getLastModTime( std::string filename ) {
    sint64 time = PHYSFS_getLastModTime( filename.c_str() );
}

PHYSFS_File *PhysFs::PhysFs::open( std::string filename, mode openMode ) {
    PHYSFS_File *file = NULL;
    switch( openMode ) {
        case READ:
            file = PHYSFS_openRead( filename.c_str() );
            break;
            
        case WRITE:
            file = PHYSFS_openWrite( filename.c_str() );
            break;
            
        case APPEND:
            file = PHYSFS_openAppend( filename.c_str() );
            break;
    }
    //cout << PHYSFS_getLastError() << endl;
    return file;
}