/* 
 * File:   PhysFsStream.hpp
 * Author: shen
 *
 * Created on 6 novembre 2013, 18:27
 */

#ifndef PHYSFSSTREAM_HPP
#define	PHYSFSSTREAM_HPP

#include <iostream>
#include <SFML/System.hpp>
#include "PhysFs.hpp"

class PhysFsStream : public sf::InputStream {
public:
    PhysFsStream( );
    PhysFsStream( std::string filename );
    virtual ~PhysFsStream();
    
    void openStream( std::string filename );
    void closeStream();
    virtual sf::Int64 read( void* data, sf::Int64 size );
    virtual sf::Int64 seek( sf::Int64 position );
    virtual sf::Int64 tell();
    virtual sf::Int64 getSize();
    bool isOpen() const;

private:
    bool m_streamOpen;
    
    PhysFs::PhysFs *m_physfs;
    PhysFs::IfStream *m_inStream;
    PHYSFS_File *m_file;
    bool m_error;

};

#endif	/* PHYSFSSTREAM_HPP */

