/*
 * File:   ConfigurationFile.cpp
 * Author: shen
 *
 * Created on 10 juillet 2013, 14:14
 */

#include "ConfigurationFile.hpp"

using namespace std;

/*ConfigurationFile::ConfigurationFile() : m_modified( false )
{
}

ConfigurationFile::ConfigurationFile( std::string fileName ) : m_fileName( fileName ),  m_modified( false )
{
}

ConfigurationFile::~ConfigurationFile() {
    if( m_modified )
        flush();
}

void ConfigurationFile::open()
{
    m_readerFile.open( m_fileName.c_str() );
    m_writerFile.open( m_fileName.c_str(), ios::out | ios::app );
    
    readFile();
}

void ConfigurationFile::open( string fileName )
{
    m_fileName = fileName;
    this->open();
}

void ConfigurationFile::eraseContents()
{
    m_readerFile.close();
    m_writerFile.close();

    ofstream( m_fileName, ios::out | ios::trunc ).close(); // Clearing file content

    //Reopening file
    open();
}

Json::Value ConfigurationFile::get( string keyName, Json::Value defaultValue )
{
    vector<string> keyList = getSubKeys( keyName );
    
    Json::Value element, value;
    element = m_rootValue;
    for( int i = 0; i < keyList.size(); ++i )
    {
        if( i == keyList.size() - 1 )
        {
            value = element.get( keyList.at( i ), defaultValue );
        }
        else
        {
            element = element[ keyList.at( i ) ];
        }
    }
    
    return value;
}

void ConfigurationFile::set( string keyName, Json::Value value )
{
    m_rootValue[ keyName ] = value;
    /*vector<string> keyList = getSubKeys( keyName );
    int size = keyList.size();
    
    if( keyList.size() > 1 )
    {
        Json::Value lastElement;
        cout << keyList.at( keyList.size() - 1) << endl;
        lastElement[ keyList.at( keyList.size() - 1) ] = value;
        //Getting first element of keyList
        Json::Value previousElement( lastElement );

        for( int i = keyList.size() - 2; i >= 1; --i )
        {
            Json::Value midValue;
            midValue[ keyList.at( i ) ] = previousElement;

            previousElement = midValue;
        }

        m_rootValue[ keyList.at( 0 ) ] = previousElement;
    }
    else
    {
        m_rootValue[ keyList.at( 0 ) ] = value;
    }*//*
    
    m_modified = true;
}

vector<string> ConfigurationFile::getSubKeys( string path )
{
    vector<string> keys;

    string buffer( "" );
    for( int i = 0; i < path.size(); ++i )
    {
        char c = path[i];
        if( path[i] != '/' )
                buffer += path[i];
        else
        {
            //buffer += '\0';
            keys.push_back( buffer );
            buffer.clear();
        }

        //Same code as above, needed when we reach the last caracter
        if( i == path.size() - 1 )
        {
            //buffer += '\0';
            keys.push_back( buffer );
            buffer.clear();
        }
    }

    return keys;
}

void ConfigurationFile::flush()
{
    eraseContents();
    m_jsonWriter.write( m_writerFile, m_rootValue );
    m_modified = false;
}

Json::Value ConfigurationFile::createElement( std::string elementName, Json::Value &parent )
{
    parent[ elementName ] = "";
    
    return parent.get( elementName, false );
}

void ConfigurationFile::readFile()
{  
    m_jsonReader.parse( m_readerFile, m_rootValue );
    
    //If rootValue is an array, so it's because of reading on an empty file
    if( m_rootValue == Json::arrayValue )
    {
        //Recreating à null rootValue to avoid horrible SIGABRT errors!
        m_rootValue = Json::Value();
    }
}*/