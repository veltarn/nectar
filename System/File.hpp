/* 
 * File:   File.hpp
 * Author: Dante
 *
 * Created on 16 octobre 2013, 01:49
 */

#ifndef FILE_HPP
#define	FILE_HPP

#include <iostream>
#include <set>
#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include "FruitException.hpp"
#include "Dir.hpp"
#include "FileInfo.h"

enum FileType {
    XML, JSON, INI
};

class File {
public:
    File( );
    File( std::string filename );
    virtual ~File( );
    
    void open( FileType fileType = INI );
    void open( std::string filename, FileType fileType = INI );
    
    void saveAsXml( std::string filename = "" );
    void saveAsJson( std::string filename = "" );
    void saveAsIni( std::string filename = "" );    
    
    
    template<typename ValueType>
    void setValue( const std::string key, const ValueType value ) {
        //Deleting row if it already exists
        m_propertyTree.put( key, value );
    }
    
    
    template<typename ValueType>
    void appendValue( const std::string key, const ValueType aValue ) {
        m_propertyTree.add( key, aValue );
    }
    
    
    template<typename ValueType>
    ValueType get( const std::string key, ValueType defaultValue ) const {        
        ValueType t = m_propertyTree.get<ValueType>( key, defaultValue );
        return t;
    }
    
    
    template<typename ValueType>
    std::set<ValueType> getArray( const std::string key ) {        
        std::set<ValueType> arraySet;

        BOOST_FOREACH( boost::property_tree::ptree::value_type v, m_propertyTree.get_child( key ) )
        arraySet.insert( v.second.data() );

        return arraySet;
    }
    
private:
    std::string m_filename;
    boost::property_tree::ptree m_propertyTree;

};

#endif	/* FILE_HPP */

