/* 
 * File:   File.cpp
 * Author: Dante
 * 
 * Created on 16 octobre 2013, 01:49
 */

#include "File.hpp"

using namespace boost::property_tree;

File::File() : m_filename()
{
}

File::File( std::string filename ) : m_filename( filename )
{
}

File::~File( )
{
}

void File::open( FileType fileType ) {
    if( Dir::exists( m_filename ) ) {
        switch( fileType ) {
            case FileType::XML:
                read_xml( m_filename, m_propertyTree );
                break;
            case FileType::JSON:
                read_json( m_filename, m_propertyTree );
                break;
            case FileType::INI:
                read_ini( m_filename, m_propertyTree );
                break;
            default:
                throw FruitException( 15, "Cannot resolve file type", FruitException::WARNING );
                break;
        }
    }
}

void File::open( std::string filename, FileType fileType ) {
    m_filename = filename;
    open( fileType );
}

/*template <typename ValueType>
void File::setValue( const std::string key, const ValueType value ) {
    //Deleting row if it already exists
    m_propertyTree.put( key, value );
}

template <typename ValueType>
void File::appendValue( const std::string key, const ValueType aValue ) {
    m_propertyTree.add( key, aValue );
}

template <typename ValueType>
void File::get( const std::string key, ValueType defaultValue ) const {
    ValueType t = m_propertyTree.get<ValueType>( key, defaultValue );
    return t;
}

template <typename ValueType>
std::set<ValueType> File::getArray(const std::string key) {
    std::set<ValueType> arraySet;
    
    BOOST_FOREACH( ptree::value_type v, m_propertyTree.get_child( key ) )
    arraySet.insert( v.second.data() );
    
    return arraySet;
}*/

void File::saveAsXml( std::string filename ) {
    if( filename == "" )
        filename = m_filename;
    
    boost::property_tree::xml_writer_settings<char> w(' ', 4);
    write_xml( filename, m_propertyTree, std::locale(), w );
}

void File::saveAsJson( std::string filename ) {
    if( filename == "" )
        filename = m_filename;
    
    write_json( filename, m_propertyTree );
}

void File::saveAsIni( std::string filename ) {
    if( filename == "" )
        filename = m_filename;
    
    write_ini( filename, m_propertyTree );
}