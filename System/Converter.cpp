/* 
 * File:   Converter.cpp
 * Author: Dante
 * 
 * Created on 11 juin 2013, 15:57
 */

#include "Converter.hpp"
#include "Size.hpp"

using namespace std;

b2Vec2 Converter::vector2fToB2Vec2( sf::Vector2f v )
{
    b2Vec2 vec;
    vec.x = v.x;
    vec.y = v.y;

    return vec;
}

b2Vec2 Converter::vector2fToB2Vec2( sf::Vector2f v, float scale )
{
    b2Vec2 vec;
    vec.x = v.x / scale;
    vec.y = v.y / scale;

    return vec;
}

/*static b2Vec2 Converter::vector2fToB2Vec2( const sf::Vector2f v )
{
    b2Vec2 vec;
    vec.x = v.x;
    vec.y = v.y;

    return vec;
}*/

sf::Vector2f Converter::b2Vec2ToVec2( b2Vec2 v )
{
    sf::Vector2f vec;
    vec.x = v.x;
    vec.y = v.y;
    
    return vec;
}

sf::Vector2f Converter::b2Vec2ToVec2( b2Vec2 v, float scale )
{
    sf::Vector2f vec;
    vec.x = v.x * scale;
    vec.y = v.y * scale;
    
    return vec;
}

int Converter::stringToInt( string v )
{
    istringstream str( v );
    int nb;
    str >> nb;
    return nb;
}

int Converter::hexaColorToInt( string v )
{
    istringstream str( v );
    int nb;
    str >> std::hex >> nb;
    return nb;
}

float Converter::stringToFloat( string v )
{
    stringstream str( v );
    float nb;
    str >> nb;
    return nb;
}

string Converter::intToString( int v )
{
    stringstream ss;
    ss << v;
    return ss.str();
}

sf::Vector2f Converter::pixelsToMeters( sf::Vector2f pixelsCoords, float worldScale )
{
    sf::Vector2f meters;
    meters.x = pixelsCoords.x / worldScale;
    meters.y = pixelsCoords.y / worldScale;
    
    return meters;
}

float Converter::pixelsToMeters( float coordPixel, float worldScale )
{
    float meters = 0.f;
    
    meters = coordPixel / worldScale;
    
    return meters;
}

sf::Vector2f Converter::metersToPixels( sf::Vector2f meters, float worldScale )
{
    sf::Vector2f pixels;
    
    pixels.x = meters.x * worldScale;
    pixels.y = meters.y * worldScale;
}

float Converter::metersToPixels( float meters, float worldScale )
{
    float pixels = 0.f;
    
    pixels = meters / worldScale;
}

sf::Vector2f Converter::sizeToVec2( const Size size )
{
    sf::Vector2f vec;
    vec.x = size.m_width;
    vec.y = size.m_height;
    
    return vec;
}

sf::Color Converter::hexaToSfColor(std::string v)
{
    //Decomposition of the string
    string strR = v.substr( 1, 2 );
    string strG = v.substr( 3, 2 );
    string strB = v.substr( 5, 2 );
    string strAlpha = "";
    //If the hexa color handle alpha channel
    if( v.size() == 9 )
    {
        strAlpha = v.substr( 7, 2 );
    }
    
    sf::Color color;
    
    color.r = Converter::hexaColorToInt( strR );
    color.g = Converter::hexaColorToInt( strG );
    color.b = Converter::hexaColorToInt( strB );
    
    if( v.size() == 9 )
        color.a = Converter::hexaColorToInt( strAlpha);
    else
        color.a = 255;
    
    return color;
}