/* 
 * File:   InputManager.hpp
 * Author: Dante
 *
 * Created on 28 juin 2013, 16:00
 */

#ifndef INPUTMANAGER_HPP
#define	INPUTMANAGER_HPP

#include <iostream>
#include <vector>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include "singleton.hpp"

class InputManager : public Singleton<InputManager>
{
    friend class Singleton<InputManager>;
public:
    void update( sf::Event event );
    
    bool keyPressed( sf::Keyboard key );
    bool keyPressed( std::vector< sf::Keyboard > keys );
    
    
private:
    sf::Event event;
};

#endif	/* INPUTMANAGER_HPP */

