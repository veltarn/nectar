/* 
 * File:   Thread.hpp
 * Author: Dante
 *
 * Created on 7 novembre 2013, 21:32
 */

#ifndef THREAD_HPP
#define	THREAD_HPP

#include <iostream>
#include <boost/thread.hpp>

class Thread {
public:
    Thread( );
    virtual ~Thread( );
    
    virtual void run() = 0;
    virtual void launch();
    void stop();
private:
    boost::thread m_thread;

};

#endif	/* THREAD_HPP */

