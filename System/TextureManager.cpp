/* 
 * File:   ResourcesManager.cpp
 * Author: Dante
 * 
 * Created on 13 juin 2013, 16:53
 */

#include "TextureManager.hpp"

TextureManager::~TextureManager()
{
    clear();
}

void TextureManager::add( std::string name, sf::Texture texture )
{
    Log *log = Log::getInstance();
    if( m_resourcesList.find( name ) == m_resourcesList.end() )
    {
        *log << "Registering \"" << name << "\"" << std::endl;
        m_resourcesList[name] = texture;
    }
    else
    {
        *log << "Ressource \"" << name << "\" already exists" << std::endl;
    }
}

void TextureManager::load( std::string path )
{
    Log *log = Log::getInstance();
    *log << "Loading \"" << path << "\"" << std::endl;
    
    sf::Texture texture;
    if( !texture.loadFromFile( path ) )
    {
        *log << "Cannot load \"" << path << "\"" << std::endl;
        throw FruitException( 5, "Cannot load \"" + path + "\"", FruitException::FATAL );
    }
    else
    {
        *log << "\"" << path << "\" has been successfully loaded" << std::endl;
    }
}

sf::Texture &TextureManager::get( std::string path )
{
    Log *log = Log::getInstance();
    
    if( m_resourcesList.find( path ) != m_resourcesList.end() )
    {
        *log << "Requesting \"" << path << "\"" << std::endl;
        return m_resourcesList[path];
    }
    else
    {
        sf::Texture texture;
        if( !texture.loadFromFile( path ) )
        {
            std::string error = "Cannot load \"" + path + "\"";
            throw FruitException( 5, error, FruitException::FATAL );
        }
        else
        {
            m_resourcesList[path] = texture;
            *log << "Registering \"" << path << "\"" << std::endl;
            return m_resourcesList[path];
        }
    }
}

bool TextureManager::exists( std::string path )
{
    for( TextureIterator it = m_resourcesList.begin(); it != m_resourcesList.end(); ++it )
    {
        if( it->first == path )
        {
            return true;
        }
    }
    return false;
}

void TextureManager::remove( std::string path )
{
    Log *log = Log::getInstance();
    
    if( m_resourcesList.find( path ) != m_resourcesList.end() )
    {
        *log << "Erasing \"" << path << "\"" << std::endl;
        m_resourcesList.erase( path );
    }
}

void TextureManager::clear()
{
    Log *log = Log::getInstance();
    
    m_resourcesList.clear();    
    *log << "Textures cache has been cleared" << std::endl;
}