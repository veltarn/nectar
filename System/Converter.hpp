/* 
 * File:   Converter.hpp
 * Author: Dante
 *
 * Created on 11 juin 2013, 15:57
 */

#ifndef CONVERTER_HPP
#define	CONVERTER_HPP

#include <iostream>
#include <sstream>
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include "Size.hpp"

class Converter {
public:
    static b2Vec2 vector2fToB2Vec2( sf::Vector2f v );
    static b2Vec2 vector2fToB2Vec2( sf::Vector2f v, float scale );
    //static b2Vec2 vector2fToB2Vec2( const sf::Vector2f v );
    static sf::Vector2f b2Vec2ToVec2( b2Vec2 v );
    static sf::Vector2f b2Vec2ToVec2( b2Vec2 v, float scale );
    static sf::Vector2f sizeToVec2( const Size size );
    
    static sf::Vector2f pixelsToMeters( sf::Vector2f pixelsCoords, float worldScale );
    static float pixelsToMeters( float coordPixel, float worldScale );
    static sf::Vector2f metersToPixels( sf::Vector2f meters, float worldScale );
    static float metersToPixels( float meters, float worldScale );
    static int stringToInt( std::string v );
    static std::string intToString( int v );
    static sf::Color hexaToSfColor( std::string v );
    static int hexaColorToInt( std::string v );
    static float stringToFloat( std::string v );
private:

};
#endif	/* CONVERTER_HPP */

