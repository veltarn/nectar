#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <fstream>
#include <ctime>
#include "singleton.hpp"
#define USER_MSG        1
#define DEBUG_MSG       2
#define WARNING_MSG     3
#define ERROR_MSG       4


class Log : public Singleton<Log>
{
    friend class Singleton<Log>;
public:
    std::string getCurrentTime();
    /*void write(std::string data);
    void write(const char  *data);*/
    
    
    Log &operator<< ( std::string text);
    Log &operator<< ( const char *text);
    Log &operator<< ( int number );
    Log &operator<< ( std::ostream & (*pf)(std::ostream&) );
private:
    std::ofstream m_file;
};

#endif // LOG_H
