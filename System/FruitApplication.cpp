/* 
 * File:   FruitApplication.cpp
 * Author: Dante
 * 
 * Created on 28 juin 2013, 14:18
 */

#include "FruitApplication.hpp"
#include "../Game/Game.hpp"

using namespace std;

/*FruitApplication::FruitApplication( string applicationName ) : m_applicationName( applicationName ), m_world( nullptr )
{
    m_log = Log::getInstance();
    *m_log << "Starting FruitEngine" << endl;
    
    init();
}


FruitApplication::~FruitApplication( )
{    
    if( m_started )
    {
        m_renderer->kill();    
        TextureManager::kill();
        *m_log << "FruitEngine stopped" << endl;    
        m_log->kill();
        m_started = false;
    }
}*/

void FruitApplication::init( string applicationName )
{    
    m_shutdownRequested = false;
    m_log = Log::getInstance();
    *m_log << "Starting FruitEngine" << endl;
    m_progressWidget = nullptr;
    m_applicationName = applicationName;   
       
    m_physFs = PhysFs::PhysFs::getInstance();
    m_physFs->init( NULL );
    initializeResourcesPath();
    
    File optionFile;
    optionFile.open( string( "data/conf/options.nopt" ), FileType::JSON );
    
    bool verticalSyncEnabled= optionFile.get<bool>( "VerticalSyncEnabled", false );
    
    m_game = nullptr;
    //Lecture dans les fichiers de conf
    m_renderWindow.create( sf::VideoMode( DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, 32 ), m_applicationName, sf::Style::Close | sf::Style::Titlebar );
    m_renderWindow.setKeyRepeatEnabled( false );
    m_renderWindow.setVerticalSyncEnabled( verticalSyncEnabled );
    m_renderWindow.resetGLStates();
    
    *m_log << "Created render window with size " << m_renderWindow.getSize().x <<  " / " << m_renderWindow.getSize().y << endl;
        
    initRenderer();
    
    setGameState( MENU );
    /*ImageEntity *image = new ImageEntity( "data/resources/images/mainMenuBackground.png" );
    m_renderer->getDrawableGroup( "background" )->push_back( image );*/
    m_guiStyles.setRootSkinDirectory( "data/gui/styles" );
    
    m_guiManager.setWindow( &m_renderWindow );
    //Setting default gui style
    // @todo: Setting conf file gui style
    if( !m_guiStyles.loadSkin( "default.nsk", *m_guiManager.getDesktop() ) ) {
        cerr << "An error has occured, unable to load window style file" << endl;
    }
    
    //Loading main menu gui
    m_guiManager.setGui( "MainMenu" );
    
    if( !m_clockFont.loadFromFile( "data/fonts/VeraMono.ttf" ) )
    {
        *m_log << "Unable to load VeraMono.ttf font" << endl;
    }
    
    m_clockHud = new ClockHud( &m_frameClock, m_clockFont );
    
    m_renderer->addEntity( "overlay", m_clockHud );
    m_gameClock.restart();
    m_started = true;
}

void FruitApplication::initRenderer()
{   
    *m_log << "Starting renderer" << endl;  
    m_renderer = Renderer::getInstance();
    m_renderer->init( );
    
    m_renderer->addDrawableGroup( "background" );    
    m_renderer->addDrawableGroup( "playground" );
    m_renderer->addDrawableGroup( "foreground" );
    m_renderer->addDrawableGroup( "overlay" );    

}

void FruitApplication::initializeResourcesPath()
{
    ifstream file;
    file.open( "resources.loc" );
    if( !file.is_open() ) {
        *m_log << "Error: Cannot open resources.loc, unable to continue, exiting" << endl;
        exit( EXIT_FAILURE );
    }
    
    while( !file.eof() ) {
        string filePath;
        getline( file, filePath );
        if( !filePath.empty() ) {
            FileInfo info( filePath );
            if( !m_physFs->mount( filePath, "/" + info.fileName(), true ) ) {
                *m_log << "Cannot mount \"" << filePath << "\" reason: " << m_physFs->getLastError() << endl;
            } else {
                *m_log <<  "\"" << filePath << "\" correctly mounted on virtual path \"/" << info.fileName() << "\"" << endl;
            }
        }
    }    
}

void FruitApplication::run()
{
    sf::Vector2u windowSize = m_renderWindow.getSize();  
    sf::View view( sf::Vector2f( windowSize.x / 2.f, windowSize.x / 2.f ) , sf::Vector2f( windowSize.x, windowSize.y ) );
    sf::View overlayView( sf::Vector2f( windowSize.x / 2.f, windowSize.y / 2.f ) , sf::Vector2f( windowSize.x, windowSize.y ) );
    
    m_renderer->getDrawableGroup( "background" )->setView( view );
    m_renderer->getDrawableGroup( "playground" )->setView( view );
    m_renderer->getDrawableGroup( "foreground" )->setView( view );
    m_renderer->getDrawableGroup( "overlay" )->setView( overlayView );
    
    while( m_renderWindow.isOpen() )
    {
        m_frameClock.beginFrame();
        
        m_frametime = m_frameClock.getLastFrameTime();
        
        processEvents( m_frametime );
        
        if( m_gameState == GameState::PLAYING)
        {            
            updateCamera(); 
            m_game->update( m_frametime );
        }
        
        m_guiManager.update( m_frametime );
        
        m_renderWindow.clear( sf::Color( 0, 0, 0 ) );
        
        if( m_gameState == GameState::PLAYING || m_gameState == GameState::MENU )
        {
            m_renderWindow.draw( *m_renderer );
        }
        
        m_guiManager.renderGui( m_renderWindow );
        
        m_renderWindow.display();
        
        m_frameClock.endFrame();
    }    
    shutdown();
}

void FruitApplication::updateCamera( ) {
    sf::View camera = m_renderer->getDrawableGroup( "playground" )->getView();
    sf::FloatRect viewport = getCameraViewport( camera );
    int xOffset = 0;
    int yOffset = 0;
    //If camera is out of world bounds
    if( viewport.left < 0 ) {
        xOffset = 0 - viewport.left;
    } else if( ( viewport.left + viewport.width ) >  m_game->getWorld()->getMap()->getMapSize().m_width  ) {
        xOffset = viewport.width - ( viewport.left + viewport.width );        
    }
    
    if( viewport.top < 0 ) {
        yOffset = 0 - viewport.top;
    } else if( ( viewport.top + viewport.height ) > m_game->getWorld()->getMap()->getMapSize().m_height ) {
        yOffset = ( viewport.height - ( viewport.top + viewport.height ) ) / 2;
    }
    camera.move( xOffset, yOffset );
    m_renderer->getDrawableGroup( "playground" )->setView( camera );
    m_renderer->getDrawableGroup( "background" )->setView( camera );
    m_renderer->getDrawableGroup( "foreground" )->setView( camera );
}

void FruitApplication::processEvents( const sf::Time delta )
{            
    while( m_renderWindow.pollEvent( m_event ) )
    {
        m_guiManager.getDesktop()->HandleEvent( m_event );
        if( m_event.type == sf::Event::Closed )
        {
            m_renderWindow.close();
        }

        if( m_event.type == sf::Event::KeyPressed )
        {
                switch( m_event.key.code )
                {
                    case sf::Keyboard::Escape:
                        unloadGame();
                    break;
                    
                    case sf::Keyboard::F1:
                        if( m_clockHud->isHidden() )
                            m_clockHud->show();
                        else
                            m_clockHud->hide();
                    break;
                }
        }
    }
    
    if( m_gameState == GameState::PLAYING)
    {
        if( sf::Keyboard::isKeyPressed( sf::Keyboard::Right ) )
        {
            int speed = 1000;
            sf::View view = m_renderer->getDrawableGroup( "playground" )->getView();
            moveCamera( view, sf::Vector2f( speed * delta.asSeconds(), 0 ) );

            m_renderer->getDrawableGroup( "playground" )->setView( view );
        }

        if( sf::Keyboard::isKeyPressed( sf::Keyboard::Left ) )
        {
            sf::Vector2f moveVector( -1000 * delta.asSeconds(), 0 );
            sf::View view = m_renderer->getDrawableGroup( "playground" )->getView();
            moveCamera( view, moveVector );

            m_renderer->getDrawableGroup( "playground" )->setView( view );
        }

        if( sf::Keyboard::isKeyPressed( sf::Keyboard::Up ) )
        {
            sf::Vector2f moveVector( 0, -1000 * delta.asSeconds() );
            sf::View view = m_renderer->getDrawableGroup( "playground" )->getView();
            moveCamera( view, moveVector );

            m_renderer->getDrawableGroup( "playground" )->setView( view );
        }

        if( sf::Keyboard::isKeyPressed( sf::Keyboard::Down ) )
        {
            sf::Vector2f moveVector( 0, 1000 * delta.asSeconds() );
            sf::View view = m_renderer->getDrawableGroup( "playground" )->getView();
            moveCamera( view, moveVector );

            m_renderer->getDrawableGroup( "playground" )->setView( view );
        }
        
        if( sf::Mouse::isButtonPressed( sf::Mouse::Left ) ) {
            if( m_gameClock.getElapsedTime().asMilliseconds() >= 250 )
            {
                sf::Vector2i mousePosition = sf::Mouse::getPosition( m_renderWindow );
                sf::View view = m_renderer->getDrawableGroup( "playground" )->getView();
                sf::FloatRect rect = getCameraViewport( view );

                Box *box = new Box( sf::Vector2f( mousePosition.x, mousePosition.y ), m_game->getWorld() );
                box->setPosition( mousePosition.x + rect.left, mousePosition.y + rect.top );
                m_game->getWorld()->addEntity( box );
                m_renderer->addEntity( "playground", box );
                cout << "Created new box on " << mousePosition.x + rect.left << "x " << mousePosition.y + rect.top << "y " << endl;
                cout << box->getPosition().x << ", " << box->getPosition().y << endl;
                m_gameClock.restart();
            }
        }
    }
    
    if( m_shutdownRequested )
    {
        //Autre code a faire (Voulez-vous vraiment éteindre bla bla bla)
        m_renderWindow.close();
    }
}

sf::RenderWindow &FruitApplication::getWindow()
{
    return m_renderWindow;
}

void FruitApplication::shutdown()
{
        if( m_game != nullptr )
        {
            unloadGame();
            delete m_game;
            m_game = nullptr;
        }
        m_renderer->kill();    
        TextureManager::kill();
        m_physFs->deinit();
        m_physFs->kill();
        *m_log << "FruitEngine stopped" << endl;    
        m_started = false;
}

World *FruitApplication::getWorld() const
{
    if( m_game != nullptr)
        return m_game->getWorld();
    else
        return nullptr;
}

void FruitApplication::moveCamera( sf::View& camera, sf::Vector2f moveFactor )
{
    //sf::FloatRect viewport = camera.getViewport();
    Size levelSize = m_game->getWorld()->getMap()->getMapSize();
    
    /*
     * Le viewport n'est qu'un simple facteur d'écran, il faut calculer le vrai
     * viewport a partir de getCenter et de la taille de la vue
     */
    
    sf::FloatRect viewport = getCameraViewport( camera );
    
    float viewportLeft = viewport.left;
    float viewportRight = viewportLeft + viewport.width;
    float viewportTop = viewport.top;
    float viewportBottom = viewportTop + viewport.height;
    
    //cout << viewportLeft << " / " << viewportRight << " / " << viewportTop << " / " << viewportBottom << endl;
    //Test if viewport get out of the level
    if( viewportLeft + moveFactor.x        >= 0 &&
        viewportTop + moveFactor.y        >= 0 &&
        viewportRight + moveFactor.x     < levelSize.m_width &&
        viewportBottom + moveFactor.y  < levelSize.m_height
      )
    {
        camera.move( moveFactor );
    }
}

sf::FloatRect FruitApplication::getCameraViewport( sf::View& camera )
{
    sf::FloatRect viewport;
    sf::Vector2f center;
    sf::Vector2f size;
    
    center       = camera.getCenter();
    size           = camera.getSize();
    
    viewport.left = center.x - ( size.x / 2 );
    viewport.top = center.y - ( size.y / 2 );
    viewport.width = size.x;
    viewport.height = size.y;
    
    return viewport;
}

void FruitApplication::setGameState( GameState state )
{
    m_gameState = state;
}

GameState FruitApplication::getGameState() const
{
    return m_gameState;
}

void FruitApplication::loadGame( string mapName )
{
    m_gameState = GameState::LOADING;
    m_progressWidget = new ProgressWidget( "Loading " + mapName, "Loading...", m_guiManager.getDesktop(), &m_renderWindow );
    unloadGame();
    
    m_game = new Game;
    m_game->setMapFile( mapName );
    
    m_thread = boost::thread( &Game::loadMap, m_game, m_progressWidget );
    //m_game->loadMap( );
}

void FruitApplication::unloadGame()
{
    if( m_game != nullptr )
    {
        m_game->unloadMap();
        delete m_game;
        m_game = nullptr;
    }
}

void FruitApplication::loadedMap()
{
    setCameraToGround();
    m_guiManager.unloadGui();
    m_guiManager.setGui( "GameGui" );
    m_gameState = GameState::PLAYING;
    delete m_progressWidget;
    m_progressWidget = nullptr;
    /*
    if( m_thread != nullptr )
    {
        m_thread->join();
        delete m_thread;
        m_thread = nullptr;
    }*/
}

void FruitApplication::unloadedMap()
{
    m_guiManager.unloadGui();
    
    m_guiManager.setGui( "MainMenu" );
    m_gameState = GameState::MENU;
}

void FruitApplication::setCameraToGround()
{
    sf::Vector2u windowSize = m_renderWindow.getSize();  
    sf::View view( sf::Vector2f( windowSize.x / 2.f, m_game->getWorld()->getMap()->getGroundPosition().y ) , sf::Vector2f( windowSize.x, windowSize.y ) );
    
    m_renderer->getDrawableGroup( "background" )->setView( view );
    m_renderer->getDrawableGroup( "playground" )->setView( view );
    m_renderer->getDrawableGroup( "foreground" )->setView( view );
}

void FruitApplication::requestShutdown()
{
    m_shutdownRequested = true;
}

string FruitApplication::getApplicationName() const
{
    return m_applicationName;
}