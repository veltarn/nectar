/* 
 * File:   Thread.cpp
 * Author: Dante
 * 
 * Created on 7 novembre 2013, 21:32
 */

#include "Thread.hpp"

Thread::Thread( )
{
}

Thread::~Thread( )
{
}

void Thread::launch() {
    try
    {
        m_thread = boost::thread( &Thread::run, this );
    } catch( boost::thread_resource_error &e ) {
        std::cerr << e.what() << std::endl;
    }
}

void Thread::stop() {
    m_thread.join();
}
