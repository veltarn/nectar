/* 
 * File:   FruitApplication.hpp
 * Author: Dante
 *
 * Created on 28 juin 2013, 14:18
 */

#ifndef FRUITAPPLICATION_HPP
#define	FRUITAPPLICATION_HPP

#define DEFAULT_WINDOW_WIDTH 1024
#define DEFAULT_WINDOW_HEIGHT 768

#include <iostream>
#include <cstdlib>
#include <fstream>
//#include <thread>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include <boost/thread.hpp>
#include "PhysFs.hpp"
#include "../Game/World.hpp"
#include "../Graphic/Window.hpp"
#include "../Graphic/Renderer.hpp"
#include "../System/TextureManager.hpp"
#include "../Graphic/DrawableGroup.hpp"
#include "../Graphic/ImageEntity.hpp"
#include "../GUI/ProgressWidget.hpp"
#include "FruitException.hpp"
#include "Log.hpp"
#include "FrameClock.hpp"
#include "../Graphic/ClockHud.hpp"
#include "Size.hpp"
#include "GUIStyles.hpp"
#include "GUIManager.hpp"
#include "File.hpp"
#include "singleton.hpp"
#include "../Debug/Box.hpp"

class Game;
enum GameState
{
    MENU, PLAYING, PAUSE, LOADING
};

class FruitApplication : public Singleton<FruitApplication>
{
    friend class Singleton<FruitApplication>;
public:
    void init( std::string applicationName );
    void requestShutdown();
    
    void run( );
    void processEvents( const sf::Time delta );
    
    sf::RenderWindow &getWindow();
    
    World *getWorld() const;
    
    /**
     * This method is used every loop cycle in order to prevent camera to get out of the world
     */
    void updateCamera( );
    void moveCamera( sf::View &camera, sf::Vector2f moveFactor );
    sf::FloatRect getCameraViewport( sf::View &camera );
    void setCameraToGround();
    
    GameState getGameState() const;
    void setGameState( GameState state );
    
    void loadGame( std::string mapName );
    void unloadGame();
    
    //EventFunction called when Game has loaded a map
    void loadedMap();
    //Event function called when Game class has unloaded a map
    void unloadedMap();
    
    std::string getApplicationName() const;
private:
    void initRenderer();
    void shutdown();
    void initializeResourcesPath();
private:
    std::string m_applicationName;
    
    Log *m_log;
    sf::Event m_event;
    Window m_renderWindow;
    Renderer *m_renderer;
    
    sf::Clock m_gameClock;
    sf::Time m_frametime;
    
    bool m_started;
    
    FrameClock m_frameClock;
    ClockHud *m_clockHud;
    sf::Font m_clockFont;
        
    GameState m_gameState;
    Game *m_game;
            
    GUIStyles m_guiStyles;
    GUIManager m_guiManager;
    ProgressWidget *m_progressWidget;
    
    bool m_shutdownRequested;
    
    PhysFs::PhysFs *m_physFs;
    
    boost::thread m_thread;
};

#endif	/* FRUITAPPLICATION_HPP */

