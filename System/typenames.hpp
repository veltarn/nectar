/* 
 * File:   typenames.hpp
 * Author: Dante
 *
 * Created on 16 octobre 2013, 18:35
 */

#ifndef TYPENAMES_HPP
#define	TYPENAMES_HPP

#include <iostream>
#include <vector>

#if defined(__LP64__) || defined(_LP64)
#define LINUX_64   1
#endif

typedef std::vector< std::vector<int > > MapData_t;
typedef std::vector< std::string > StringList;

#endif	/* TYPENAMES_HPP */

