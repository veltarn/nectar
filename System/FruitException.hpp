/* 
 * File:   FruitException.hpp
 * Author: Dante
 *
 * Created on 11 juin 2013, 15:06
 */

#ifndef FRUITEXCEPTION_HPP
#define	FRUITEXCEPTION_HPP

#include <iostream>
#include <exception>

class FruitException : public std::exception
{
public:
    enum ERROR_LEVEL
    {
        MINOR, WARNING, CRITICAL, FATAL
    };

    FruitException( int errorNumber = 0, std::string text="",  ERROR_LEVEL errorLevel = FruitException::MINOR) throw()
            : m_errorNumber(errorNumber), m_textError(text), m_errorLevel(errorLevel)
    {}

    virtual const char* what() const throw()
    {
        return m_textError.c_str();
    }

    int getErrorNumber() const throw()
    {
        return m_errorNumber;
    }

    FruitException::ERROR_LEVEL getErrorLevel() const throw()
    {
        return m_errorLevel;
    }

    std::string errorLevelToString(FruitException::ERROR_LEVEL errorLevel) throw()
    {
        std::string errorLvl;
        switch(errorLevel)
        {
            case FruitException::MINOR:
                errorLvl = "[MINOR] ";
            break;

            case FruitException::WARNING:
                errorLvl = "[WARNING] ";
            break;

            case FruitException::CRITICAL:
                errorLvl = "[CRITICAL] ";
            break;

            case FruitException::FATAL:
                errorLvl = "[FATAL] ";
            break;
        }

        return errorLvl;
    }

    virtual ~FruitException( ) throw()
    {}
private:
    int m_errorNumber;
    std::string m_textError;
    FruitException::ERROR_LEVEL m_errorLevel;
};
#endif	/* FRUITEXCEPTION_HPP */

