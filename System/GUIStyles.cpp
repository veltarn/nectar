/* 
 * File:   GUIStyle.cpp
 * Author: shen
 * 
 * Created on 6 juillet 2013, 15:34
 */

#include "GUIStyles.hpp"

using namespace std;
using namespace boost::property_tree;

GUIStyles::GUIStyles() {
    m_log = Log::getInstance();
}

GUIStyles::GUIStyles( std::string rootDirectory ) {
    m_rootSkinDirectory = rootDirectory;
    m_log = Log::getInstance();
        
    parseDirectory();
}

GUIStyles::~GUIStyles() {
}


void GUIStyles::setRootSkinDirectory(std::string dir)
{
        m_rootSkinDirectory = dir;
        parseDirectory();
}

std::string GUIStyles::getRootSkinDirectory() const
{
        return m_rootSkinDirectory;
}

void GUIStyles::parseDirectory()
{
        Dir dir( m_rootSkinDirectory );
        
        vector<string> extensions;
        extensions.push_back( ".nsk" );
        
        m_skinsList = dir.entryList( Dir::FILES, extensions );        
        *m_log << "Parsed gui directory \"" << m_rootSkinDirectory << "\", found " << m_skinsList.size() << " skin files." << endl;
}

bool GUIStyles::loadSkin( string skinName, sfg::Desktop& desktop )
{
    return desktop.LoadThemeFromFile( skinName );
    //Verifying existence of skinName in m_skinsList array
    /*for( vector<string>::iterator it = m_skinsList.begin(); it != m_skinsList.end(); ++it )
    {
        if( *it == skinName  )
        {         
            vector<GUIProperty> properties;
            try
            {
                properties = parseFile( skinName );
            }
            catch( FruitException &e )
            {
                *m_log << e.what() << endl;
            }
            
            applyTheme( properties, desktop );
            return true;
        }
    }    
    return false;*/
}

vector<GUIProperty> GUIStyles::parseFile( std::string fileName )
{
    /*ifstream file( m_rootSkinDirectory + "/" + fileName, ios::in ); 
    Json::Value root;
    Json::Reader jsonFile;
    
    if( !jsonFile.parse( file, root ) )
    {
        throw FruitException( 8, jsonFile.getFormatedErrorMessages(), FruitException::CRITICAL );
    }
    
    vector<GUIProperty> guiProperties;
    Json::Value propertiesNode = root.get( "Properties", Json::Value() );
    
    for( int i = 0; i < propertiesNode.size(); ++i )
    {
        GUIProperty property;
        Json::Value row = propertiesNode.get( i, Json::Value() );
        
        property.widgetName = row.get( "WidgetName", Json::Value() ).asString();
        property.property = row.get( "Property", Json::Value() ).asString();
        property.type = row.get( "Type", "Int" ).asString();
        property.value = row.get( "Value", "0" ).asString();
        
        guiProperties.push_back( property );
    }
    
    return guiProperties;*/
}

void GUIStyles::applyTheme( vector<GUIProperty> &propertiesArray, sfg::Desktop &desktop )
{
    for( vector<GUIProperty>::iterator it = propertiesArray.begin(); it != propertiesArray.end(); ++it )
    {
        GUIProperty cProperty = *it;
                
        if( cProperty.type == "Int" )
        {
            int value = Converter::stringToInt( cProperty.value );
            desktop.SetProperty( cProperty.widgetName, cProperty.property, value );
        }
        else if( cProperty.type == "Float" )
        {
            float value = Converter::stringToFloat( cProperty.value );
            desktop.SetProperty( cProperty.widgetName, cProperty.property, value );
        }
        else if( cProperty.type == "String" )
        {
            desktop.SetProperty( cProperty.widgetName, cProperty.property, cProperty.value );            
        }
        else if( cProperty.type == "SfColor" )
        {
            sf::Color value = Converter::hexaToSfColor( cProperty.value );
            desktop.SetProperty( cProperty.widgetName, cProperty.property, value );
        }
        else
        {
            std::cerr << "[ ERROR ] Unrecognized style type" << std::endl;
        }
    }
    *m_log << "Theme was successfully applied" << endl;
}