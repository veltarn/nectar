/* 
 * File:   GUIManager.hpp
 * Author: shen
 *
 * Created on 7 juillet 2013, 00:39
 */

#ifndef GUIMANAGER_HPP
#define	GUIMANAGER_HPP

#include <iostream>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFGUI/SFGUI.hpp>
#include "../Graphic/Window.hpp"
#include "../GUI/MainMenu.hpp"

class GUIManager {
public:
    GUIManager();
    virtual ~GUIManager();
    
    void update( sf::Time delta );
    void renderGui( sf::RenderWindow &window );
    
    /*
        ... find something better... much better...
     */
    void setGui( std::string guiName );
    AbstractGui *getCurrentGui() const;
    
    void setWindow( Window *window );
    //Used to change Gui style
    sfg::Desktop *getDesktop();
    
    void unloadGui();
private:
    sfg::Desktop m_desktop;
    Window *m_window;
    sfg::SFGUI m_sfgui;
    
    AbstractGui *m_currentGui;
};

#endif	/* GUIMANAGER_HPP */

