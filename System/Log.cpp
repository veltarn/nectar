#include "Log.hpp"

using namespace std;


/*void Log::write(string data, int userMsg)
{
    cout << getCurrentTime() << data << endl;
}*/

/*void Log::write(char *data, int msgType = USER_MSG)
{
    cout << getCurrentTime() << data << endl;
}*/

string Log::getCurrentTime()
{
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "%H:%M:%S", timeinfo);
    std::string tmp(buffer);
    std::string ftime = "[" + tmp + "] ";

    return ftime;
}

Log &Log::operator<< ( string text )
{
    if( !m_file.is_open() )
    {
        m_file.open( "fruitEngineDebug.txt" );
    }
    string logText = getCurrentTime() + text;
    m_file << text;
#ifdef FRUIT_DEBUG
    cout << text;
#endif
    m_file.flush();
    return *this;
}

Log &Log::operator<< ( const char *text )
{
    if( !m_file.is_open() )
    {
        m_file.open( "fruitEngineDebug.txt" );
    }
    
    m_file << text;
#ifdef FRUIT_DEBUG
    cout << text;
#endif
    m_file.flush();
    return *this;
}

Log &Log::operator<< ( int number )
{
    if( !m_file.is_open() )
    {
        m_file.open( "fruitEngineDebug.txt" );
    } 
    
    try
    {
        m_file << number;
#ifdef FRUIT_DEBUG
        cout << number;
#endif
    }
    catch( ios_base::failure &e)
    {
        cout << e.what() << endl;
    }
    m_file.flush();
    return *this;
}

Log &Log::operator<< ( ostream & (*pf)(ostream&) )
{
    if( !m_file.is_open() )
    {
        m_file.open( "fruitEngineDebug.txt" );
    } 
    
    m_file << pf;
#ifdef FRUIT_DEBUG
    cout << pf;
#endif
    return *this;
}