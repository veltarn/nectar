/* 
 * File:   GUIManager.cpp
 * Author: shen
 * 
 * Created on 7 juillet 2013, 00:39
 */

#include "GUIManager.hpp"

GUIManager::GUIManager() : m_currentGui( nullptr )
{
}

GUIManager::~GUIManager() {
    if( m_currentGui != nullptr )
    {
        delete m_currentGui;
        m_currentGui = nullptr;
    }
}

void GUIManager::setGui( std::string guiName )
{
    //Freeing current gui
    if( m_currentGui != nullptr )
    {
        m_currentGui->freeInterface();    
        delete m_currentGui;
        m_currentGui = nullptr;
    }
    
    if( guiName == "MainMenu" )
    {
        MainMenu *menu = new MainMenu( &m_desktop, m_window );
        m_currentGui = menu;
    }
}

AbstractGui *GUIManager::getCurrentGui() const
{
    return m_currentGui;
}

void GUIManager::update( sf::Time delta )
{
    m_desktop.Update( delta.asSeconds() );
}

void GUIManager::renderGui( sf::RenderWindow& window )
{
    m_sfgui.Display( window );
}

sfg::Desktop *GUIManager::getDesktop()
{
    return &m_desktop;
}

void GUIManager::setWindow( Window *window )
{
    m_window = window;
}

void GUIManager::unloadGui()
{
    if( m_currentGui != nullptr )
    {
        delete m_currentGui;
        m_currentGui = nullptr;
        
        m_desktop.RemoveAll();
    }
}