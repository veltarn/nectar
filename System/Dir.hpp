/* 
 * File:   Dir.h
 * Author: Dante
 *
 * Created on 20 février 2013, 17:54
 */

#ifndef DIR_H
#define	DIR_H

#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
/**
 * @brief Classe représentant un répertoire
 * @version 0.5
 */
class Dir {
    
public:    
    enum Filter
    {
        FILES = 0x0001,
        DIRECTORIES = 0x0002,
        ALL = FILES | DIRECTORIES
    };
    
public:
    /**
     * @brief Constructeur par défaut prenant une chaîne de caractère en paramètre
     * @param rootPath Chaîne de caractères représentant le répertoire
     */
    Dir(const char *rootPath);
    /**
     * @brief Constructeur surchargé permettant de prendre un std::string à la place d'une chaîne de caractères
     * @param rootPath std::string représentant le répertoire
     */
    Dir(std::string rootPath);
    Dir(const Dir& orig);

    /**
     * @brief liste toutes les entrées du répertoire courant
     * @return vecteur de std::string comportant la liste des fichiers de ce répertoire
     */
    std::vector<std::string> entryList( Dir::Filter filter = Dir::ALL, std::vector<std::string> extensionsFilter = std::vector<std::string>() );

    std::string absoluteFilePath();
    static bool exists( std::string path );
    
    virtual ~Dir();
private:
    void initPath();
    bool allowedExtension( boost::filesystem::path &filename, std::vector<std::string> allowedExtensions );
    
private:
    boost::filesystem::path m_rootPath; // Chemin racine qui est indiqué dans le constructeur
};
#endif	/* DIR_H */

