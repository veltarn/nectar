/* 
 * File:   Size.cpp
 * Author: Dante
 * 
 * Created on 8 juin 2013, 19:10
 */

#include "Size.hpp"

using namespace std;


Size::Size() : m_width(0), m_height(0)
{
}

Size::Size( float width, float height ) : m_width( width ), m_height( height )
{}

Size::Size( const Size& orig ) {
    m_width = orig.m_width;
    m_height = orig.m_height;
}

Size::~Size() {
}

void Size::display( ostream& stream ) const
{
    stream << "Size: [ " << m_width << " x " << m_height << " ]";
}

void Size::setSize(float width, float height)
{
    m_width = width;
    m_height = height;
}

float Size::getWidth() const
{
    return m_width;
}

void Size::setWidth(float width)
{
    m_width = width;
}

float Size::getHeight() const
{
    return m_height;
}

void Size::setHeight(float height)
{
    m_height = height;
}

Size Size::operator +( Size const &sizeB )
{
    Size copy(*this);
    copy += sizeB;
    return copy;
}

Size Size::operator-( Size const &sizeB )
{
    Size copy(*this);
    copy -= sizeB;
    return copy;
}

Size Size::operator*( Size const &sizeB )
{
    Size copy(*this);
    copy *= sizeB;
    return copy;
}

Size Size::operator /( Size const &sizeB )
{
    Size copy(*this);
    copy /= sizeB;
    return copy;
}

Size &Size::operator +=( const Size& other )
{
    m_width     += other.m_width;
    m_height    += other.m_height;

    if( m_width < 0 )
        m_width = 0;
    else if( m_height < 0 )
        m_height = 0;

    return *this;
}

Size &Size::operator -=( const Size& other )
{
    m_width     -= other.m_width;
    m_height    -= other.m_height;

    if( m_width < 0 )
        m_width = 0;
    else if( m_height < 0 )
        m_height = 0;

    return *this;
}

Size &Size::operator *=( const Size& other )
{
    m_width     *= other.m_width;
    m_height    *= other.m_height;

    if( m_width < 0 )
        m_width = 0;
    else if( m_height < 0 )
        m_height = 0;

    return *this;
}

Size &Size::operator /=( const Size& other )
{
    if( other.m_width == 0 || other.m_height == 0 )
    {
        //Exception
    }

    m_width     /= other.m_width;
    m_height    /= other.m_height;

    if( m_width < 0 )
        m_width = 0;
    else if( m_height < 0 )
        m_height = 0;

    return *this;
}

Size &Size::operator =( const Size& other )
{
    m_width = other.m_width;
    m_height = other.m_height;

    return *this;
}

bool Size::operator ==( Size const &b )
{
    if(m_width == b.m_width && m_height == b.m_height)
        return true;
    else
        return false;
}

bool Size::operator !=( Size const &b )
{
    if( m_width == b.m_width && m_height == b.m_height )
        return false;
    else
        return true;
}

bool Size::operator <( Size const &b )
{
    if( m_width < b.m_width && m_height < b.m_height )
        return true;
    else
        return false;
}

bool Size::operator >( Size const &b )
{
    if( m_width > b.m_width && m_height > b.m_height )
        return true;
    else
        return false;
}

bool Size::operator <=( Size const &b )
{
    if( m_width <= b.m_width && m_height <= b.m_height )
        return true;
    else
        return false;
}

bool Size::operator >=( Size const &b )
{
    if( m_width >= b.m_width && m_height >= b.m_height )
        return true;
    else
        return false;
}

ostream &operator<<( ostream &stream, Size const &size )
{
    size.display( stream );
    return stream;
}