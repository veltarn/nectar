/* 
 * File:   PhysFsStream.cpp
 * Author: shen
 * 
 * Created on 6 novembre 2013, 18:27
 */

#include "PhysFsStream.hpp"

using namespace std;

PhysFsStream::PhysFsStream() : m_streamOpen( false ), m_physfs( nullptr ), m_inStream( nullptr ), m_file( NULL ), m_error( false ) {

}


PhysFsStream::PhysFsStream( string filename ) : m_streamOpen( false ), m_physfs( nullptr ), m_inStream( nullptr ), m_error( false ) {
    openStream( filename );
}

PhysFsStream::~PhysFsStream() {
    if( m_inStream != nullptr )
        delete m_inStream;
    
    if( !m_error ) {
        if( m_file != NULL ) {
            PHYSFS_close( m_file );
        }
    }
}

void PhysFsStream::openStream( string filename ) {
    /*m_physfs = PhysFs::PhysFs::getInstance();
    m_inStream = new PhysFs::IfStream( filename );
    if( !m_inStream->isOpen() ) {
        cerr << m_physfs->getLastError() << endl;
    } else {
        m_streamOpen = true;
    }*/
    m_file = PHYSFS_openRead( filename.c_str() );
    if( m_file == NULL ) {
        cerr << PHYSFS_getLastError() << endl;        
        m_error = true;
    }
}

void PhysFsStream::closeStream() {
    /*if( m_inStream->isOpen() ) {
        delete m_inStream;
        m_inStream = nullptr;
        m_streamOpen = false;
    }*/
    if( !m_error ) {
        if( m_file != NULL ) {
            PHYSFS_close( m_file );
            m_file = NULL;
        }
    }
}

sf::Int64 PhysFsStream::read( void* data, sf::Int64 size ) {
    /*if( m_inStream->isOpen() ) {
        m_inStream->read( (char*)data, size );
        if( m_inStream )
            return size;
        else
        {
            cerr << m_physfs->getLastError() << endl;
            return -1;
        }
    }
    cerr << "Cannot read in the stream: Stream not open" << endl;
    return -1;*/
    if( m_error )
        return -1;
    
    sf::Int64 readStream = PHYSFS_read( m_file, (char*)data, 1, size );
    if( readStream == -1 ) {
        cerr << PHYSFS_getLastError() << endl;
        return -1;
    }
    return readStream;
}

sf::Int64 PhysFsStream::seek( sf::Int64 position ) {
    /*if( m_inStream->isOpen() ) {
        m_inStream->seekg( position, ios::beg );
        if( m_inStream )
            return position;
        else
            return -1;
    }
    return -1;*/
    if( m_error )
        return -1;
    
    if( PHYSFS_seek( m_file, position ) == 0 ) {
        cerr << PHYSFS_getLastError() << endl;
        return -1;
    }
    return position;
}

sf::Int64 PhysFsStream::tell() {
    /*if( m_inStream->isOpen() ) {
        sf::Int64 pos = m_inStream->tellg();
        return pos;
    }
    
    return -1;*/
    
    if( m_error ) 
        return -1;
    
    sf::Int64 pos = PHYSFS_tell( m_file );
    if( pos == -1 ) {
        cerr << PHYSFS_getLastError() << endl;
        return -1;
    }
    
    return pos;
}

sf::Int64 PhysFsStream::getSize() {
    /*if( m_inStream->isOpen() ) {
        streampos curPos = m_inStream->tellg();
        m_inStream->seekg( 0, ios::end );
        sf::Int64 size = m_inStream->tellg();
        m_inStream->seekg( curPos, ios::beg );
        
        return size;
        
    }
    return -1;*/
    if( m_error )
        return -1;
    
    sf::Int64 size = PHYSFS_fileLength( m_file );
    if( size == -1 )
    {
        cerr << PHYSFS_getLastError() << endl;
        return -1;
    }
    return size;
}

bool PhysFsStream::isOpen() const {
    //return m_streamOpen;
    if( m_file != NULL )
        return true;
    else
        return false;
}