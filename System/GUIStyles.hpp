/* 
 * File:   GUIStyles.hpp
 * Author: shen
 *
 * Created on 6 juillet 2013, 15:34
 */

#ifndef GUISTYLES_HPP
#define	GUISTYLES_HPP

#include <iostream>
#include <fstream>
#include <SFGUI/SFGUI.hpp>
#include <SFML/Graphics.hpp>
#include "File.hpp"
#include "Log.hpp"
#include "Converter.hpp"
#include "Dir.hpp"
#include "FruitException.hpp"

struct GUIProperty
{
    std::string widgetName;
    std::string property;
    std::string type;
    std::string value;
};

class GUIStyles 
{
public:
    GUIStyles();
    GUIStyles( std::string rootDirectory );
    virtual ~GUIStyles();
    
    
    void setRootSkinDirectory( std::string dir );
    std::string getRootSkinDirectory() const;
    
    bool loadSkin( std::string skinName, sfg::Desktop &desktop );
    void parseDirectory();
    
    //This method will parse and load into the desktop
   std::vector<GUIProperty> parseFile( std::string fileName );
   
   void applyTheme( std::vector<GUIProperty> &propertiesArray, sfg::Desktop &desktop );
private:
    std::string m_rootSkinDirectory;
    std::vector< std::string > m_skinsList;
    Log *m_log;
};

#endif	/* GUISTYLES_HPP */

