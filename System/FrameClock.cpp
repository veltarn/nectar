/* 
 * File:   FrameClock.cpp
 * Author: Dante
 * 
 * Created on 3 juillet 2013, 14:36
 */

#include "FrameClock.hpp"

FrameClock::FrameClock( std::size_t depth )
{
    assert(depth >= 1);
    m_sample.data.resize(depth);

    m_freq.minimum = detail::limit<float>();
    m_time.minimum = sf::microseconds(detail::limit<sf::Int64>());
}

void FrameClock::clear()
{
    FrameClock( getSampleDepth() ).swap( *this );
}

void FrameClock::beginFrame()
{
    m_clock.restart();
}

sf::Time FrameClock::endFrame()
{
    m_time.current = m_clock.getElapsedTime();

    m_sample.accumulator -= m_sample.data[m_sample.index];
    m_sample.data[m_sample.index] = m_time.current;

    m_sample.accumulator += m_time.current;
    m_time.elapsed       += m_time.current;

    if ( ++m_sample.index >= getSampleDepth() )
    {
        m_sample.index = 0;
    }

    if ( m_time.current != sf::microseconds( 0 ) )
    {
        m_freq.current = 1.0f / m_time.current.asSeconds();
    }

    if ( m_sample.accumulator != sf::microseconds(0) )
    {
        const float smooth = static_cast<float>( getSampleDepth() );
        m_freq.average = smooth / m_sample.accumulator.asSeconds();
    }

    const sf::Int64 smooth = static_cast<sf::Int64>( getSampleDepth() );
    m_time.average = sf::microseconds( m_sample.accumulator.asMicroseconds() / smooth );

    if ( m_freq.current < m_freq.minimum )
        m_freq.minimum = m_freq.current;
    if ( m_freq.current > m_freq.maximum )
        m_freq.maximum = m_freq.current;

    if ( m_time.current < m_time.minimum )
        m_time.minimum = m_time.current;
    if ( m_time.current > m_time.maximum )
        m_time.maximum = m_time.current;

    ++m_freq.elapsed;

    return m_time.current;
}

void FrameClock::setSampleDepth(std::size_t depth)
{
    assert(depth >= 1);
    FrameClock(depth).swap(*this);
}

std::size_t FrameClock::getSampleDepth() const
{
    return m_sample.data.size();
}

sf::Time FrameClock::getTotalFrameTime() const
{
    return m_time.elapsed;
}

sf::Uint64 FrameClock::getTotalFrameCount() const
{
    return m_freq.elapsed;
}

sf::Time FrameClock::getLastFrameTime() const
{
    return m_time.current;
}

sf::Time FrameClock::getMinFrameTime() const
{
    return m_time.minimum;
}

sf::Time FrameClock::getMaxFrameTime() const
{
    return m_time.maximum;
}

sf::Time FrameClock::getAverageFrameTime() const
{
    return m_time.average;
}

float FrameClock::getFramesPerSecond() const
{
    return m_freq.current;
}

float FrameClock::getMinFramesPerSecond() const
{
    return m_freq.minimum;
}

float FrameClock::getMaxFramesPerSecond() const
{
    return m_freq.maximum;
}

float FrameClock::getAverageFramesPerSecond() const
{
    return m_freq.average;
}

void FrameClock::swap( FrameClock& other)
{
    this->m_time.swap( other.m_time );
    this->m_freq.swap( other.m_freq );
    this->m_sample.swap( other.m_sample );
    std::swap( this->m_clock, other.m_clock );
}